{{#*inline "page"}}
<p><strong>Note:</strong> I <a href="/blogs/adding-webmentions">stopped accepting comments</a> in favour of webmentions, this post is here for archiving purposes.</p>

<p>My <a href="/projects/website">website</a> is written in <code>Rust</code> with <code>Rocket</code> framework. But, just like any other website, what language or framework I use doesn't matter here. The source code can be found at my <a href="https://codeberg.org/codingotaku/website/" rel="external" {{{settings.opener.metadata}}}>codeberg repository</a>.</p>

<h2 id="sqlite-for-db">Using SQLite for database</h2>
<p>This was a hard one to choose, I have many things planned for this website, most of which would require a high performing database. But I ended up using <code>SQLite</code> as it is easy to maintain and work on. Taking backups is done by a simple <code>rsync</code> command!</p>

<p>I created a table named <code>comments</code> and I set all columns as text, including the ID as I will be using <a href="https://datatracker.ietf.org/doc/html/rfc4122" rel="external" {{{settings.opener.metadata}}}>UUID</a> (Universally Unique IDentifier) for it. The SQL looked something like this:</p>
{{#code lang='sql'}}CREATE TABLE IF NOT EXISTS comments (
  id TEXT NOT NULL, -- random UUID
  post TEXT NOT NULL, -- URL for the page
  name TEXT NOT NULL, -- Optional name, defaulted to anonymous
  email TEXT, -- optional email
  website TEXT, -- optional website for the commenter
  comment TEXT NOT NULL, -- the comment
  commenter TEXT NOT NULL, -- random UUID
  reply_to TEXT -- something for the future threaded replies
){{/code}}

<p>You might have noticed that I did not tell SQLite to set the default value for the <code>name</code> field with {{#inline-code}}name TEXT DEFAULT 'anonymous'{{/inline-code}}. This is because of a bug I faced in the rust SQLite library I am using, the library was not converting <code>Option::None</code> as <code>NULL</code> in SQLite. So instead, I am <a rel="external" {{{settings.opener.metadata}}} href="https://codeberg.org/codingotaku/website/src/commit/28c9ffa434f44e20a10870feb41672ba6c818f80/src/modals/comments.rs#L131">programmatically setting</a> the default value for now.</p>

<p>The comments are sent as a <a href="https://codeberg.org/codingotaku/website/src/commit/28c9ffa434f44e20a10870feb41672ba6c818f80/src/routes/main_pages.rs#L282" rel="external" {{{settings.opener.metadata}}}>post request</a>, I do some request guards there to avoid spam, more on that in the next section.</p>

<h2 id="spam-control">Spam control</h2>
<p>One of the major concerns I have had about implementing a comment system was the spam control. I believe that I have very low traffic for this website as I don't have <a href="/blogs/why-do-i-not-use-analytics">analytics to confirm</a>, and I <a href="/privacy">delete access logs fairly frequently</a>.
</p>

<p>I initially just accepted all the comments. Well, you could probably guess what happened, a low traffic website means that there are more bots crawling my site than humans visiting it. I frequently received spam messages, most containing links to some pharmacy in India, and some were just trolls. The links were easy to handle, I just show plain text in comments, so none of the links were rendered as <abbr title="HyperText Markup Language">HTML</abbr> to avoid the visitors and search engines navigating to it.</p>

<p>My first plan for moderation was to send a push notification to my android phone whenever a comment was added to the website so that I can delete it quickly. I wanted to do it using <a href="https://unifiedpush.org/" rel="external" {{{settings.opener.metadata}}}>UnifiedPush</a> as I already have <a href="https://codeberg.org/NextPush/uppush" rel="external" {{{settings.opener.metadata}}}>NextPush</a> server hosted in my <a href="https://nextcloud.com/" rel="external" {{{settings.opener.metadata}}}>Nextcloud</a> instance. Don't think that it is too much work, if you have a notification server set-up, it is just a matter of sending a <code>POST</code> request <a href="https://codeberg.org/codingotaku/website/src/commit/d2ecf2a51e2fbd1f10cd72014f4bd0388caac0d9/src/routes/main_pages.rs#L310" rel="external" {{{settings.opener.metadata}}}>like I did</a>. If you do not have a server to set one up, something like <a href="https://ntfy.sh/" rel="external" {{{settings.opener.metadata}}}>ntfy.sh</a> might interest you.</p>

<p>Sending myself a notification is obviously not a good way to do moderation, but it worked for me, as the frequency of spams reduced as I started deleting them immediately.</p>

<p>Just because the spams reduced doesn't mean that I did good moderation, I was still getting spams at least once a week. It was then I decided to ask a question to the commenter, it's a simple one to answer if they are human. And even if the answer is wrong, I return an <abbr title="HyperText Transfer Protocol">HTTP</abbr> <a href="https://codeberg.org/codingotaku/website/src/commit/d2ecf2a51e2fbd1f10cd72014f4bd0388caac0d9/src/routes/main_pages.rs#L290" rel="external" {{{settings.opener.metadata}}}>redirect response</a>, this will trick the bots into thinking that they succeeded. I went with this approach as I would rather not implement captcha, there are no good accessibility friendly captchas out there!</p>

<p>I never received a spam since then, I probably have missed a few real comments if the visitor couldn't answer the question. It can be solved by adding the wrong answers to a separate table or something and moderating it later. But I don't think it is that important for my small and humble website.</p>

<p>In the future, I want the visitors to be able to reply to comments. I already have some work done for that in the backend, it will probably be released along with the <a href="https://indieweb.org/Webmention" rel="external" {{{settings.opener.metadata}}}>webmention</a> support I am working on at the moment!</p>

<p>The posted comments will be hidden until I review and <a href="https://codeberg.org/codingotaku/website/src/commit/28c9ffa434f44e20a10870feb41672ba6c818f80/src/routes/admin.rs#L189" rel="external" {{{settings.opener.metadata}}}>approve it</a>. But it would be a problem for the commenter if they can't see their own comment. So, for a slightly better user experience, the commenters can now see their comments even if it is not moderated yet, they can also edit or delete it if they'd like to.</p>

<figure class="figure">
  <img class="img" loading="lazy" src="/images{{page.url}}/comment-example.webp" alt="" width="817" height="443">
  <figcaption class="image-caption"><p>Screenshot of a comment waiting for moderation, with links to edit and delete it.</p></figcaption>
</figure>

<p>This is done by storing <a href="https://codeberg.org/codingotaku/website/src/commit/28c9ffa434f44e20a10870feb41672ba6c818f80/src/routes/main_pages.rs#L294" rel="external" {{{settings.opener.metadata}}}>a unique ID</a> in a <a href="https://codeberg.org/codingotaku/website/src/commit/28c9ffa434f44e20a10870feb41672ba6c818f80/src/routes/main_pages.rs#L300" rel="external" {{{settings.opener.metadata}}}>private cookie</a> when someone comments for the first time and using it to connect to all their comments from then onwards. The ID is a randomly generated <abbr title="Universally Unique IDentifier">UUID</abbr>.</p>

<p>My final SQL for the comment database looks like this:</p>
{{#code lang='sql'}}CREATE TABLE IF NOT EXISTS comments (
  id TEXT NOT NULL, -- random UUID
  post TEXT NOT NULL, -- URL for the page
  name TEXT NOT NULL, -- Optional name, defaulted to anonymous
  email TEXT, -- optional email
  website TEXT, -- optional website for the commenter
  comment TEXT NOT NULL, -- the comment
  commenter TEXT NOT NULL, -- random UUID
  reply_to TEXT, -- something for the future threaded replies
  date TEXT NOT NULL -- the created date, I could use the DATE type here though
  is_moderated INTEGER NOT NULL, -- Integer because SQLite doesn't support boolean.
                                 -- 1 means that the comment is moderated.
){{/code}}

<p>For now, I think this is sufficient for my needs, even providing comments is already an overkill for my small website.</p>

{{/inline}}
{{> (lookup this 'parent') }}

