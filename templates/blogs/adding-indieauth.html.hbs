{{#*inline "page"}}
<p class="note"><strong>Note:</strong> Most code examples in this page are in <a href="https://www.rust-lang.org/" rel="external" {{{settings.opener.metadata}}}>Rust</a> and in the <a href="https://handlebarsjs.com/" rel="external" {{{settings.opener.metadata}}}>Handlebars template</a>. But that shouldn't be a problem to follow this, the base logic remains the same, regardless of what language you use, the Rust just helps me avoid a few unnecessary checks.</p>

<p>In my <a href="/blogs/adding-webmentions">last post</a>, I wrote about adding <a href="https://www.w3.org/TR/2017/REC-Webmention-20170112/" rel="external" {{{settings.opener.metadata}}}>Webmentions</a> to this website. It ended with me saying that I will add <a href="https://www.w3.org/TR/indieauth" rel="external" {{{settings.opener.metadata}}}>IndieAuth</a> support next. But as I was busy being laid off and searching for a job (I'm still searching for one), I couldn't really find much time to do it. But I also wanted a new blog post to come out at every month, so here I am trying to keep my promise.</p>

<h2 id="basics">The basics</h2>
<p>The first step is to read the <a href="https://www.w3.org/TR/indieauth/" rel="external" {{{settings.opener.metadata}}}>IndieAuth specification</a>. I will ignore the client-side parts in this blog post, as they are irrelevant to me.</p>

<p>What I am going to do is a <strong>single-user authentication</strong>. Since I am not allowing others to login through my website, the process is even simpler. It should be possible to change this implementation to handle multi-user authentication without much effort.</p>

<p>For us to login to other IndieWeb webistes/apps (clients), we need an <a href="https://www.w3.org/TR/indieauth/#authorization-endpoint" rel="external" {{{settings.opener.metadata}}}>authorization endpoint</a>, and to allow publishing through a client, we need a <a href="https://www.w3.org/TR/indieauth/#token-endpoint" rel="external" {{{settings.opener.metadata}}}>token endpoint</a>.</p>

<p>Currently, I publish all posts on my website as <a href="https://codeberg.org/codingotaku/website/src/branch/main/templates" rel="external" {{{settings.opener.metadata}}}>HTML files</a> in handlebars template format. Due to this, <strong>token endpoint</strong> does not make sense for me to implement right now, but I am updating this website more regularly now, so it might be a thing I do in the future!</p>

<h2 id="authentication">Authentication</h2>
<p>Authentication is the act of validating that users are whom they claim to be. The IndieAuth specification <a href="https://www.w3.org/TR/indieauth/#x5-authentication" rel="external" {{{settings.opener.metadata}}}>tells us how this works</a>, let us note it down first.</p>

<p>The endpoint that handles the request is an <strong>authorization endpoint</strong> instead of an <strong>authentication endpoint</strong>. This is because both authentication and authorization go through the same route at the beginning.</p>

<p class="note">In the following list, the client would be a website or app that I am logging on to, like <a href="https://indieweb.org" rel="external" {{{settings.opener.metadata}}}>indieweb.org</a>.</p>

<ol>
  <li>I enter my <a href="https://www.w3.org/TR/indieauth/#user-profile-url" rel="external" {{{settings.opener.metadata}}}>profile URL</a> in the login form of the client and click "Sign in"</li>
  <li>The client discovers my <strong>authorization endpoint</strong> by fetching my <strong>profile URL</strong> and looking for the {{#inline-code}}rel=authorization_endpoint{{/inline-code}} value.</li>
  <li>The client builds the <a href="https://www.w3.org/TR/indieauth/#authentication-request" rel="external" {{{settings.opener.metadata}}}>authentication request</a> including its <a href="https://www.w3.org/TR/indieauth/#client-identifier" rel="external" {{{settings.opener.metadata}}}>client identifier</a>, <strong>local state</strong> (a random string), and a <a href="https://www.w3.org/TR/indieauth/#redirect-url" rel="external" {{{settings.opener.metadata}}}>redirect URI</a>, and redirects the browser to the <strong>authorization endpoint</strong> I am creating now.</li>
  <li>The <strong>authorization endpoint</strong> fetches the client information from the client identifier URL to have an application name and icon to display to me. (I might will this and just show the URLs to avoid complexity)</li>
  <li>The <strong>authorization endpoint</strong> prompts me to log in and asks whether to grant or deny the client's authentication request.</li>
  <li>The <strong>authorization endpoint</strong> generates an <strong>authorization code</strong> and redirects the browser back to the client, including the <strong>local state</strong> in the URL. This is called an <a href="https://www.w3.org/TR/indieauth/#authentication-response" rel="external" {{{settings.opener.metadata}}}>authentication response</a>.</li>
  <li>The client <a href="https://www.w3.org/TR/indieauth/#authorization-code-verification" rel="external" {{{settings.opener.metadata}}}>verifies the authorization code</a> by making a POST request to the authorization endpoint. The authorization endpoint validates the authorization code, and <a href="https://www.w3.org/TR/indieauth/#response" rel="external" {{{settings.opener.metadata}}}>responds</a> with the End-User's canonical profile URL.</li>
</ol>

<p>You can see that half of the steps are done from the client side, so it is not as complex as it looks.</p>

<p>The <code>authorization code</code> we need to generate could be anything, the specification has not mandated a format or length, so I am thinking of just using a <a href="https://en.wikipedia.org/wiki/Universally_unique_identifier" rel="external" {{{settings.opener.metadata}}}>UUID</a> for this.</p>

<h3 id="implement-authentication">Implementing Authentication</h3>
<p>Now I know what needs to be done, and where to look for the specification, so it's time to do some coding.</p>

<h4 id="updating-login">Updating the existing login</h4>
<p>I have an <a href="https://codeberg.org/codingotaku/website/src/commit/cd9029301d0bd7d317cecb948efbeaf5cfe48340/src/routes/admin.rs#L196" rel="external" {{{settings.opener.metadata}}}>existing login implementation</a>, this is a username and password-based authentication and I use it for moderating webmentions (previously comments). The password is hashed and salted. Once logged in, <a href="https://codeberg.org/codingotaku/website/src/commit/5d8e3f81540c839c11117d25778be7752de10e10/src/routes/admin.rs#L217" rel="external" {{{settings.opener.metadata}}}>I store the login status</a> in a private cookie.</p>

<p>To support the authorization confirmation screen that I will develop later, I need to modify the login page to redirect to a <code>URL path</code> when present, so I will start with that.</p>

<p>First, I will accept a new optional string parameter named {{#inline-code}}redirect_path{{/inline-code}} in the login request parameter, I will use this to redirect to the <code>authorization endpoint</code>.</p>

{{#code}}
#[get("/login-to-site?<redirect_path>")]
async fn login_page(redirect_path: Option<&str>, ... ) -> Template {
  ...
    Template::render("admin/login-to-site",
      ...
      page {
        ...
        redirect_path
        ...
      }
      ...
    )
 ...
}
{{/code}}

<p>I used the <code>redirect_path</code> in my template as a hidden input element. Maybe I should inform myself about this redirection, but I don&apos;t think that will be a problem as there are more manual steps to be done afterwords and I also do security checks later.</p>
{{#code}}
\{{#if page.redirect_path}}
    <input type="hidden" name="redirect_path" value=\{{page.redirect_path}}>
\{{/if}}
{{/code}}

<p>If you are interested, you can see these changes in my <a a href="https://codeberg.org/codingotaku/website/compare/cd9029301d0bd7d317cecb948efbeaf5cfe48340..bc0b142b68792fcaa163c5ea82cf70fd86134c5f" rel="external" {{{settings.opener.metadata}}}>commit history</a>.</p>

<p>I now need to redirect to this URL when the login POST request is successful, but I do not want to redirect to any page that comes up in the URL. I need all redirections to be within my website domain. This is as simple as making sure that the path starts with a forward slash (<code>/</code>). So I wrote a helper method to handle all in-page redirections.</p>

{{#code}}
fn redirect_to_path(mut optional_path: Option<String>, default: String) -> Redirect {
  let path = optional_path.take_if(|path| path.starts_with("/"));
  Redirect::to(path.unwrap_or(default))
}
{{/code}}


<p>The idea is to make the <strong>login page</strong> redirect back to the <strong>authorization page</strong>, and do a final confirmation within the authorization page that will do the <strong>authentication response</strong>.</p>

<h4 id="authentication-page">Creating the Authentication page</h4>
<p>This is the page we would land on after trying to sign in through a client using my domain URL. I want this to be as simple as possible, and I am also thinking of adding the <strong>token endpoint</strong> support to it later.</p>

<p>Anyway, I went back and read the specification again to get an example of what we will get from the client, and it is a <code>GET</code> request like this:</p>
{{#code}}https://example.org/auth?me=https://user.example.net/&
                          redirect_uri=https://app.example.com/redirect&
                          client_id=https://app.example.com/&
                          state=1234567890&
                          response_type=id
{{/code}}

<p>Looking at the specification, I know the following:</p>
<ul>
  <li>The parameter {{#inline-code}}me{{/inline-code}} should be my domain URL in its canonical form (e.g. {{#inline-code}}https://codingotaku.com/{{/inline-code}}).</li>
  <li>The {{#inline-code}}redirect_uri{{/inline-code}} is the URI I need to redirect to after authorization.</li>
  <li>The {{#inline-code}}client_id{{/inline-code}} is the URI of the app</li>
  <li>The {{#inline-code}}state{{/inline-code}} is a value that I need to send to the client as it is to avoid cross-site request forgery.</li>
  <li>And finally, the optional {{#inline-code}}response_type{{/inline-code}} that defaults to <code>id</code> means that this is an authentication request.</li>
</ul>

<p>If the {{#inline-code}}response_type{{/inline-code}} is {{#inline-code}}code{{/inline-code}}, that means that it is an <strong>authorization</strong> request instead of an <strong>authentication</strong> request, we will handle this later, for now, I will treat all requests as an <strong>authentication</strong> request.</p>

<p>Ideally, the {{#inline-code}}redirect_uri{{/inline-code}} would be using the same host and port as the {{#inline-code}}client_id{{/inline-code}}. But this might not be the case always. So we need to ensure that the client supports the {{#inline-code}}redirect_uri{{/inline-code}} provided by crawling the client. But I decided to just accept those requests in the initial version, and add that in after finding a good HTML parsing library for rust.</p>

<p id="back-ref-1">As the <a href="https://www.w3.org/TR/indieauth/#x4-2-1-application-information" rel="external" {{{settings.opener.metadata}}}>specification tells</a>, it is important to show as much detail as possible in authorization page. My idea is to display the <code>response_type</code>, <code>client_id</code>, and the <code>redirect_uri</code> on the page. So that I can do a manual verification before logging in. This way, I can catch the abnormalities, like <code>client_id</code> and <code>redirect_uri</code> not matching.</p>

<p>The page code will look something like this (written in handlebars template):</p>
{{#code}}
<p>You are receiving a request to authenticate to <a href="\{{page.client_id}}">\{{page.client_id}}</a></p>
<p>After authentication, this page will be redirected to <code>\{{page.redirect_uri}}</code></p>
<p>If this request looks suspicious, please manually verify them, or avoid authenticating the request.</p>

<h2>Request details</h2>
<dl>
  <dt><strong>Client ID</strong></dt>
  <dd>\{{page.client_id}}</dd>
  <dt><strong>Redirect URI</strong></dt>
  <dd>\{{page.redirect_uri}}</dd>
  <dt><strong>Response type</strong></dt>
  <dd>\{{page.response_type}}</dd>
</dl>
{{/code}}

<p>The authentication page needs to check whether I am logged in or not, and If I am logged in, a button should be shown to do the authentication; otherwise, I should be asked to log in.</p>

{{#code}}
\{{#if settings.is_logged_in}}
<form class="form" action="/authentication" method="post" accept-charset="utf-8" aria-label="Authentication">
  <input type="hidden" name="redirect_uri" value=\{{page.redirect_uri}}>
  <input type="hidden" name="response_type" value=\{{page.response_type}}>
  <input type="hidden" name="state" value=\{{page.state}}>
  <input type="hidden" name="client_id" value=\{{page.client_id}}>
  <input type="hidden" name="me" value=\{{page.me}}>
  <button type="submit" class="submit-button">Authenticate</button>
</form>
\{{else}}
  <p class="space-out"><a class="link-button" href="/login-to-site?redirect_path=\{{page.escaped_uri}}">Login to Authenticate this request</a></p>
\{{/if}}
{{/code}}

<p>Here is a screenshot of the authentication page before logging in:</p>

<figure class="figure">
  <picture>
    <source class="img" srcset="/images{{page.url}}/authentication-page.webp" width="790" height="588" type="image/webp">
    <img class="img" src="/images{{page.url}}/authentication-page.png" width="790" height="588" alt="Screenshot before logging in" loading="lazy">
  </picture>
  <figcaption class="image-caption">
    <p>The authentication request page has the client ID, redirect URI, and the response type with a link to login. I placed a warning to not proceed if the request looks suspicious.</p>
  </figcaption>
</figure>

<p>After logging in, the only change is that I replaced the login link with a button to authenticate the request, here is the screenshot:</p>

<figure class="figure">
  <picture>
    <source class="img" srcset="/images{{page.url}}/authentication-page-after-login.webp" width="785" height="579" type="image/webp">
    <img class="img" src="/images{{page.url}}/authentication-page-after-login.png" width="785" height="579" alt="Screenshot after logging in" loading="lazy">
  </picture>
  <figcaption class="image-caption">
    <p>Same content as the previous screenshot, with the link replaced with an <q>Authenticate</q> button placed in the centre.</p>
  </figcaption>
</figure>

<p>The complete page template can be found <a href="https://codeberg.org/codingotaku/website/src/commit/5d8e3f81540c839c11117d25778be7752de10e10/templates/authentication-page.html.hbs" rel="external" {{{settings.opener.metadata}}}>in my repository</a>.</p>

<h4 id="table">Creating a table to store access codes</h4>
<p>To keep track of the authentication requests, I will create a new table named <code>indie_auth_code</code>. Within this, I will add all <strong>six</strong> columns that I will be needing for authentication.</p>

<ol>
  <li><code>id</code>: for me to query them later.</li>
  <li><code>client_id</code>: The client requesting for authentication.</li>
  <li><code>redirect_uri</code>: The redirect URI send by the client.</li>
  <li><code>code</code>: a unique authentication/authorization code.</li>
  <li><code>created_on</code>: The date and time of creating this row.</li>
  <li><code>expire_on</code>: the date and time when the code expires.</li>
</ol>

<p>I use <a href="https://www.sqlite.org/" rel="external" {{{settings.opener.metadata}}}>SQLite</a> for storing things on my website, and the table schema looks like this:</p>
{{#code}}CREATE TABLE indie_auth_code (
  id TEXT NOT NULL,
  client_id TEXT NOT NULL,
  redirect_uri TEXT NOT NULL,
  code TEXT NOT NULL,
  created_on DATETIME NOT NULL,
  expires_on DATETIME NOT NULL,
  UNIQUE(code)
);
{{/code}}

<h4 id="authentication-endpoint">Authentication endpoint</h4>
<p>Now that we have a table, we reached the main part of the whole process. It is time to do the actual authentication.</p>

<p>I created a new endpoint named <code>authentication</code>. This receives a <code>POST</code> request that contains all values that will be sent by the client. The response will always be a redirection.</p>

{{#code}}
#[derive(FromForm)]
pub(crate) struct IndieAuthForm {
    pub me: String,
    pub client_id: String,
    pub redirect_uri: String,
    pub state: String,
    pub response_type: Option<String>,
}

#[post("/authentication", data = "<indie_auth>")]
async fn authentication(indie_auth: Form<IndieAuthForm>) -> Redirect {
  ...          
}
{{/code}}

<p>You will notice that <code>reponse_type</code> is an <code>Option</code>, this is because it is an optional parameter that defaults to the string <code><q>id</q></code>.</p>

<p>The <code>authentication</code> endpoint will first ensure that both the <code>client_id</code> and <code>rediect_uri</code> are of the same host and port. I wrote a few helper methods for this:</p>

{{#code}}
fn is_port_matching(source: Option<u16>, target: Option<u16>) -> bool {
    (source.is_none() && target.is_none())
        || (source.is_some_and(|_| target.is_some()) && source.unwrap().eq(&target.unwrap()))
}

fn is_host_matching(source: &str, target: &str) -> bool {
    !(source.is_empty() || target.is_empty() || source.ne(target))
}

fn get_url_authority(uri_value: &str) -> Option<Authority> {
    let url = uri::Absolute::parse(uri_value);

    url.map_or(None, |url| url.authority().cloned())
}

fn is_authority_matching(source_uri: &str, target_uri: &str) -> bool {
    let source = get_url_authority(source_uri);
    let target = get_url_authority(target_uri);

    source.is_some_and(|source_val| {
        target.is_some_and(|target_val| {
            is_host_matching(source_val.host(), target_val.host())
                && is_port_matching(source_val.port(), target_val.port())
        })
    })
}

{{/code}}

<p>The helper methods will handle all edge cases of varying ports and host names, but if they differ, I will need to <a href="https://www.w3.org/TR/indieauth/#redirect-url" rel="external" {{{settings.opener.metadata}}}>crawl the client to find the redirect_uri</a>. As I <a href="#back-ref-1">mentioned before</a>, I do this manually now. I'll update this blog once I find time to do a proper check.</p>

<p>Once the first check was done, I generated a new <abbr title="Universally unique identifier">UUID</abbr>, and stored it in the <code>indie_auth_code</code> table we created before. We already have all the details we need, the <code>created_on</code> is the current time and <code>expires_on</code> will be around 5 minutes past the current time.</p>

{{#code}}
let code = Uuid::new_v4().to_string();
let now = OffsetDateTime::now_utc();

let auth_code = IndieAuthCode {
    id: Uuid::new_v4().to_string(),
    client_id: indie_auth.client_id.clone(),
    redirect_uri: indie_auth.redirect_uri.clone(),
    code: code.clone(),
    created_on: now.format(&well_known::Iso8601::DEFAULT).unwrap(),
    expires_on: now
        .checked_add(Duration::minutes(5))
        .unwrap()
        .format(&well_known::Iso8601::DEFAULT)
        .unwrap(),
};

add_auth_code(&mut db, &auth_code).await;
{{/code}}

<p>The <a href="https://www.w3.org/TR/indieauth/#x5-3-authentication-response" rel="external" {{{settings.opener.metadata}}}>specification suggests</a> a maximum of 10 minutes for the expiration time, but let us play a little safe and keep it at 5. I want it to change the time limit via a config or something in the future.</p>

<p>On hindsight, setting time is something <a href="https://www.sqlite.org/lang_datefunc.html" rel="external" {{{settings.opener.metadata}}}>I could do from the database</a> itself, but this is what we are going with right now.</p>
  
<p>I sent it to <code>redirect_uri</code> along with the given <code>state</code> as an HTTP redirect with <code>302 Found</code> status.</p>

<p>You can find the full implementation with more error handling <a href="https://codeberg.org/codingotaku/website/src/commit/2ca1531c21263a436a1bb4feba7407dd928ddd5e/src/routes/indie_auth.rs#L142" rel="external" {{{settings.opener.metadata}}}>in my repository</a>.</p>

<p>Everything is coming together, now as per the specification, I need to accept another <a href="https://www.w3.org/TR/indieauth/#x5-4-authorization-code-verification" rel="external" {{{settings.opener.metadata}}}>Authorization Code Verification</a> request being sent by the client to the <strong>authorization endpoint</strong>.</p>

<h4 id="auth-code-verification">Authorization Code Verification</h4>
<p>The validation part is simple, the client will give us the <code>code</code>, <code>client_id</code>, and the <code>redirect_uri</code> as the <code>POST</code> parameter. We just need to return the profile URL as a <code>JSON</code> to the client after the validating it against the details we saved in the last step within the <code>indie_auth_code</code> table. My response will need to look like this:</p>

{{#code}}
{
  "me": "https://codingotaku.com/"
}
{{/code}}

<p>In case of errors, I am just sending the error code instead, something like this:</p>
{{#code}}
{
  "error": "invald_request"
}
{{/code}}

<p>All validations can be done from the database itself, so I wrote this query:</p>
{{#code}}
SELECT id, client_id, redirect_uri, code, created_on, expires_on, is_accessed
       FROM indie_auth_code
       WHERE code=[the-received-cde]
       AND client_id=[received-client-id]
       AND redirect_uri=[received-redirect-uri]
       AND datetime(expires_on) > datetime('now');
{{/code}}

<p>The query is being used by my <code>authorization</code> endpoint, I also do the redundant checks to ensure that the request is valid before doing the query, this is not really needed, but it can prevent an unnecessary query.</p>

{{#code}}
#[post("/indie-auth", data = "<code_verification>")]
async fn auth_verification(
    mut db: Connection<Db>,
    code_verification: Form<IndieAuthCodeVerification>,
    config: &State<AppConfig>,
) -> (Status, Json<IndieAuthResponse>) {
    if !is_authority_matching(
        &code_verification.client_id,
        &code_verification.redirect_uri,
    ) {
        return (
            Status::BadRequest,
            Json(IndieAuthResponse {
                me: None,
                error: Some(String::from("invalid_request")),
            }),
        );
    }

    if let Ok(auth_code) = get_auth_code(&mut db, &code_verification).await {
        delete_auth_code(&mut db, &auth_code.id).await;
        (
            Status::Found,
            Json(IndieAuthResponse {
                me: format!("{}/", config.card.homepage).into(),
                error: None,
            }),
        )
    } else {
        (
            Status::NotFound,
            Json(IndieAuthResponse {
                me: None,
                error: Some(String::from("invalid_grant")),
            }),
        )
    }
}
{{/code}}

<h4 id="using-endpoint">Using the authorization endpoint</h4>
<p>The final step is to actually use this endpoint. Until now, I was using <code>indieauth.com/auth</code> as the endpoint, I updated it to the new <code>codingotaku.com/indie-auth</code>.</p>

<p>Note: If you go to that page without proper request parameters, I will just show you a <strong>404 not found</strong> page, this is to trick some of the bad bots that I've been getting.</p>

<p>If you would like to see the complete set of changes, <a href="https://codeberg.org/codingotaku/website/compare/cd9029301d0bd7d317cecb948efbeaf5cfe48340..2ca1531c21263a436a1bb4feba7407dd928ddd5e" rel="external" {{{settings.opener.metadata}}}>here is the commit difference</a> between the implementations (you'll need to scroll down a bit to see the files).</p>

<h3 id="issues">The issues I faced</h3>
<p>My initial approach was to reject the <strong>authentication request</strong> if the <code>response_type</code> is not <q>id</q> or empty. But all the clients I tried to log in sent me the <code>response_type</code> as <q>code</q> instead. Annoyingly, it is also true for the <a href="https://indieweb.org" rel="external" {{{settings.opener.metadata}}}>indieweb.org</a>. It could either be a bug, or they have more features that I can unlock once I add a <strong>token endpoint</strong>.</p>

<p>In my <a href="https://codeberg.org/codingotaku/website/src/commit/2ca1531c21263a436a1bb4feba7407dd928ddd5e/src/routes/indie_auth.rs#L163" rel="external" {{{settings.opener.metadata}}}>current implementation</a>, I am just printing this on to the console and ignoring it (I know, shut up). But I will need to handle this by creating a <strong>token endpoint</strong> later.</p>

<p>Other than this one thing, I haven't really faced any other issues, hope this helps, now I am having 100% IndieWeb implementation done by myself without depending on a service!</p>

<h2 id="next-steps">Next steps</h2>
<p>As I have mentioned a few times in the article, I am still missing a <strong>token endpoint</strong>. But I have some work already being done on a separate branch to use a database for all the posts. It is a big task as I need to think of better backup systems in case I corrupt the DB.</p>

<p>Once the database work is done, I will be able to add a token endpoint and post without updating the repository all the time <span class="ucode">😄</span>.</p>
{{/inline}}
{{> (lookup this 'parent') }}

