{{#*inline "page"}}
<p>This is part two of the series on writing a website, in the part one, <a href="/blogs/writing-a-website-part-1"><q>Recipe for Making a Website</q></a>, I wrote about <strong><em>what</em> we can use</strong> to write a website, <strong>but not <em>how</em> to write one</strong>.</p>

<p>The images in here might be unreadable when reading on devices with small screen size. This is because the images are screenshots taken from a laptop screen and resized, I have provided sufficient (or I believe they are sufficient) descriptions for them. Ideally, you should be able to get the same results in your system if you do the steps I mention in this article. If you still need to see the images, please open them in a new tab.</p>
<p>A website is usually created to provide information to a visitor. Which is typically written in text format. There are many ways to send this information to the visitor, but let us not focus on that for now, what we need is to write that information down somehow so that it can be shared.</p>

<p>But wait, shouldn't we figure out <em>how to share the information</em> first so that we can write it down in a way that it can be shared?</p>

<p>It might seem like a chicken-and-egg problem, but fortunately for us, this is already solved. There is an existing and widely used protocol named <abbr>HTTP</abbr> (Hypertext Transfer Protocol) that supports transferring information through the internet, and that protocol supports a markup language called <abbr title="HyperText Markup Language">HTML</abbr>, which is what we are going to use today.</p>

<p>
  HyperText Markup Language or <abbr>HTML</abbr> is the standard markup language for documents designed to be displayed in a web browser. It defines the content and structure of web content.
</p>

<p>HTML is a complex and evolving markup language, but it is easy to use because there are very few rules you need to learn to write a full-fledged website.</p>

<h2 id="structure">Structuring the web page</h2>
<p>In HTML, the structure is defined using <q>tags</q>, a tag in HTML is usually written in the format <code>&lt;tag-name&gt;</code> this is called an opening tag. An opened tag can be closed by writing the same tag and adding a <code>/</code> after <code>&lt;</code> (e.g. <code>&lt;/tag-name&gt;</code>).</p>

<p>An <q>element</q>, or to better word it, an <q>HTML element</q>, <em>often</em> consists of opening and closing tags. Everything in between those tags are called <q>content</q> of that element. For example, <code>&lt;q&gt;flower&lt;/q&gt;</code> creates a <code>quote</code> element, and it is rendered as <samp><q>flower</q></samp> — here <q>flower</q> is the content.
</p>

<p>
  You can place an <i>element</i> within another <i>element</i>, this is called <q>nesting</q>. But what we call it is not relevant, all you need to know is that you can infinitely nest elements! But be careful, the more nested the elements are, the more you burden the visitor's browser and their RAM, so we must try to reduce nesting as much as possible.
</p>

<p>Some elements cannot have any child (nested) element or text contents, they are called <q>void</q> elements. Void elements only have a start tag; end tags must not be specified for void elements. An example of this is the <code>&lt;input&gt;</code> tag.</p>

<p>Some HTML formatting tools (like prettier) try to <q>self-close</q> void tags by inserting a trailing <code>/</code> after the tag name (<code>&lt;input/&gt;</code>), there are some historical reasons for that, like supporting XML parsers where there are no concepts of <q>void</q> tags.</p>

<p>Fun fact, no browser will complain or behave weirdly if you self-close a void tag (citation needed). But self-closing is <strong>not</strong> a concept in HTML, so any tool trying to insert a self-closing tag in HTML to support XML parsers is fighting with two things that do not support each other.</p>


<p>Though no error will be thrown, there are some more restrictions on what elements can be placed in another, but let us not worry about that for now and learn different elements that we can use.</p>

<h3 id="semantics">Scemantic elements</h3>
<p>
  Most elements convey semantic meaning, and purpose. A <code>&lt;p&gt;</code> tag is used to create a paragraph element, for example. This paragraph is written in HTML, like this.
</p>
{{#code lang="html"}}<p>
  Most elements convey semantic meaning, and purpose. A <code>&lt;p&gt;</code> tag is used to create a paragraph element, for example. This paragraph is written in HTML, like this.
</p>
{{/code}}
<p>Noticed the weird <code>&amp;lt;p&amp;gt;</code>? In HTML, if you want to write <code>&lt;</code> or <code>&gt;</code> as text, you need to escape them. This can be done by writing <code>&amp;lt;</code> and <code>&amp;gt;</code>. So <code>&amp;lt;p&amp;gt;</code> will create the text <code>&lt;p&gt;</code> instead of creating a tag. Modern IDEs will probably highlight them so that it will be easier to read.</p>

<p>There are many more characters that you can escape like this, you might be interested in reading about <a href="https://www.w3.org/International/questions/qa-escapes" rel="external" {{{settings.opener.metadata}}}>using character escapes in markup and CSS</a>.</p>

<p>A good website with lots of content will have more paragraph elements than any other elements, but showing just plain text would be a bit boring and might not suit everyone's needs or taste. There are elements to style texts within a paragraph, and some of these do similar things, but you can use them based on their semantic meaning.</p>

<p>For example, both <code>&lt;i&gt;</code> and <code>&lt;em&gt;</code> tags are rendered (displayed) by the browser by making its content italic.
  Scemantically, <code>&lt;i&gt;</code> can be used to make a text <i>italic</i> in style, but to <em>emphasize</em> a text, we should use <code>&lt;em&gt;</code> tag instead.
</p>
<p>Other similarly styled tags are <code>&lt;b&gt;</code> and <code>&lt;strong&gt;</code>, <strong>while both of them render the text in bold</strong>, <code>&lt;strong&gt;</code> tag is used to show strong text that has a <q>strong importance</q>. And <code>&lt;b&gt;</code> tag is for, as you guessed, to make the text bold (technically, increasing the font weight).</p>

<p>
  While paragraphs are important, it is also important to have heading and subheading for the document, this makes it easier for the reader to traverse through the document.
  <code>&lt;h1&gt;</code> tag is used to show the heading of the document, a document must have <strong>only one</strong> of them (though as usual, the browser won't stop you from having more).
</p>
<p>
  You can create a subheading by increasing the number after the <q>h</q> in the heading tag, at present, it is possible to have subheadings from <code>h2 to h6</code>. When using them, one must not skip a heading level, i.e a <code>h3</code> must not be created if there is no <code>h2</code> tag present before it. The higher the number, the smaller the text, but as I already mentioned, it is used to write headings and subheadings, and <em>not</em> to style texts.
</p>

<h3 id="dom">Writing an HTML document</h3>
<p>
  Now that we learned <em>how</em> to create text content, let us focus on putting this into a document and see it from a browser.
</p>

<p>
  As I mentioned at the beginning, the HTML is an evolving language, currently <code>HTML5</code> is widely used, it has support for more semantic HTML elements than its prior versions. The browser must know the document format and version that we are using, this is done by using the <code>&lt;!DOCTYPE&gt;</code> declaration.

  Did you notice that I wrote <q>declaration</q> and not <q>tag</q>? This is because the declaration is not an HTML tag. It is <q>information</q> to the browser about what document type to expect.
</p>

<p>
  Okay, now that we know what to do, let us create this document for real this time.
</p>


<p>To create an HTML document, any text editing software can be used. BUT, we are going to write plain text here, so it is better if you don't use a word processor. Use a simple text editor, like ed, vi, vim, Emacs, Atom, Notepad++, or in worst-case use Sublime Text, or VS Code. But if you have opened Microsoft Word, close it, we don't need it here.
</p>

<p>
  Create a new folder in your system — name it whatever you feel like, it is not relevant — now create an empty file within that folder named {{#inline-code}}index.html{{/inline-code}}, this time, the name is important, make sure you spelled it correctly.
</p>

<p>
  The name <code>index.html</code> is usually looked up by most tools that can be used as a server (homework, find the historical reasoning behind it). Though we are not using any such tools at this point, we will need it in the future, so let us stick to that naming convention for now.
</p>
<p>
  After creating the empty file, open it using your text editor and write the DOCTYPE to it. For HTML5, the DOCTYPE is written as {{#inline-code}}<!DOCTYPE html>{{/inline-code}} (yes, it is not a typo, it is <code>html</code> and not <code>html5</code>).
</p>

<p>
  The HTML document has a few tags that need to be placed to be rendered properly. The first one is <code>&lt;html&gt;</code>, this is the root element of the HTML document, and there should be only one root element present in an HTML document.
</p>

<p>Your file should now look like this:</p>
{{#code lang="html"}}
<!DOCTYPE html>
<html>
</html>
{{/code}}

<p>
  Inside the <code>&lt;html&gt;</code> element, we can place two elements, one is <code>&lt;head&gt;</code> which can contain some additional information about the file, like the title of the document, information about the author, a brief description, how the document should be rendered, etc. For now, let us just add a title.
</p>
{{#code lang="html"}}
<!DOCTYPE html>
<html>
  <head>
    <title>A simple HTML document</title>
  </head>
</html>
{{/code}}

<p>Simple? I will add two more things.</p>
<p>To support more languages, we can tell the browser to use the <code>UTF-8</code> character set when rendering the text by placing this <code>meta</code> tag within the <code>head</code> element: 
  {{#inline-code}}<meta charset="UTF-8">{{/inline-code}}.
</p>
<p>To tell what language the webpage is using to the browser, and any assistive technology the visitor uses, we can use the <code>lang</code> attribute in the <code>html</code> tag: 
  {{#inline-code}}<html lang="en">{{/inline-code}}.
</p>
<p>Just like my website, I am using the International English here, for US english, you will need to use <code>en-us</code> instead. To find the correct code for the language you are using, try the <a href="https://r12a.github.io/app-subtags/" rel="external" {{{settings.opener.metadata}}}>Language Subtag Lookup</a>. <strong>You must do this because an empty lang tag means that the language is <code>undefined</code></strong>.</p>
<p>
  The other element we can place inside the <code>&lt;html&gt;</code> is the  <code>&lt;body&gt;</code> element, all contents that we write should be placed within the <code>body</code>.
</p>

{{#code lang="html"}}
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <title>A simple HTML document</title>
  </head>
  <body>
  </body>
</html>
{{/code}}

<p>While we can place all the contents within the body, it is a good practice to further divide the sections. For someone using assistive technology, it would be nice to have indication of different <strong>sections</strong> of the page, we call them <strong>landmarks</strong>. We are not going to use them now, just keep it in your mind, by the end of this series you will learn when and how to use them.</p>

<p>What you have now created is called an <q>HTML boilerplate</q>, you can copy that file content and when creating more HTML files. Some IDEs (Integrated Development Environments) can generate this (and a bit more) for you, it is up-to you to figure that out.
</p>
<p>
  Now that the boilerplate is ready, let us put some content in it after remembering the rules:
</p>
<ul>
  <li>A document must have only one <code>h1</code> tag.</li>
  <li>Subheadings must not skip levels.</li>
  <li>All paragraphs must use <code>p</code> tag.</li>
  <li>Ensure that the document follows a semantic structure.</li>
</ul>

{{#code lang="html"}}
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <title>A simple HTML document</title>
  </head>
  <body>
    <h1>Welcome to my simple HTML page</h1>
    <p>
      An HTML page is used for sharing information with others. And to ensure that <em>everyone</em> can access the page, we must be conscious of the <strong>semantic</strong> and <strong>accessibility</strong> when writing it.
    </p>
  </body>
</html>
{{/code}}

<p>Now save the file with the above content, once saved, you can open the <code>index.html</code> file using your web browser. There are many ways to do this, one way is to just drag the <code>index.html</code> file into the web browser. Or you might be able to right-click on the file and open it using the default browser (this flow varies between OS and Desktop environments).
</p>

<p>Once you have opened the file in the browser, you might see something like this screenshot.</p>
<figure class="figure">
  <picture>
    <source class="img" srcset="/images{{page.url}}/index-html.webp" width="1052" height="437" type="image/webp">
    <img class="img" src="/images{{page.url}}/index-html.png" width="1052" height="437" alt="Firefox screenshot" loading="lazy">
  </picture>
  <figcaption class="image-caption">
    <p>Firefox tab with <q>A simple HTML document</q> as title. The webpage contains black text in white background. The heading says <q>Welcome to my simple HTML page</q>, and it is rendered as a big and heavy text.</p>
    <p>The paragraph below the heading has the text <q>everyone</q> rendered in italic style. The texts <q>semantic</q> and <q>accessibility</q> are rendered in bold letteres.</p>
  </figcaption>
</figure>


<h2 id="styles">Styling the web page</h2>
<p>That screenshot above was very plain looking, and if you are reading my website in one of its dark themes, you <i>might</i> have strained your eyes, so how about we make it a bit better?</p>
<p>Unlike the structure of the website, styling is not a compulsory thing to do. But just like the structure, one should be mindful of the semantic and accessibility of the page when styling an HTML page.</p>

<p>One of the main things that many still get wrong is the colour contrast, the default colour scheme of your website must have proper contrast. Remember, if you create a public website, or share links to it, you are effectively asking others to read it, it should not be a burden for them.</p>

<p>Fixing colour contrast used to be a hard thing to do, but now there are many tools that can help one to achieve good colour contrast. One of them is <a href="https://webaim.org/resources/contrastchecker/" rel="external" {{{settings.opener.metadata}}}>WebAIM: Contrast Checker</a>.</p>

<p>Now that we have decided to be inclusive of people with needs, let us learn how to style a web page!</p>

<p>
  There are multiple ways to style elements in HTML:
</p>

<ul>
  <li>Using inline styles by using the <code>style</code> <em>attribute</em></li>
  <li>Using the <code>&lt;style&gt;</code> <em>tag</em> inside <code>&lt;head&gt;</code> <em>tag</em>.</li>
  <li>Using a stylesheet, which is just the contents of the <code>&lt;style&gt;</code> placed in a separate file.</li>
</ul>
<p>We will be using a stylesheet, which is easier to read, write, and manage. The other ways I mentioned to style HTML documents are usually discouraged as it makes managing styles hard, but feel free to explore how to do them on your own.</p>

<h3 id="stylesheets">Stylesheets</h3>
<p>
  A stylesheet is a simple text file that contain styles in CSS (Cascading Style Sheet) format. The purpose of CSS is to change the look and feel of the web page, but as you learn more about it, you might get tempted to ignore the HTML structure completly, we will try to avoid that here.
</p>

<p>CSS is not just for styling HTML, it can be used to style SVG or XML documents, and some graphical user interface toolkits also supports styling via CSS, so it is always good to learn a bit about it.</p>

<p>
  For styles to work, we need to to tell the browser where to look for it. This is done by adding a <code>link</code> tag within the <code>head</code>. The syntax will look like {{#inline-code}}<link rel="stylesheet" href="/path/to/style.css">{{/inline-code}}. As you might have noticed, <code>link</code> is a void tag, you do not need to close it.
</p>

<h4 id="syntax">The syntax</h4>
<p>The CSS has a very simple syntax, a <strong>selector</strong>, <strong>declaration block</strong>, <strong>properties</strong>, and <strong>values</strong>. There are also some more advanced things like <strong>media queries</strong>, but the basics remains the same.</p>

If you decide to have the main heading on your page to be shown in the center with red text, the following code shows a very simple CSS rule that would achieve that styling:
{{#code lang='css'}}
h1 {
  color: red;
  text-align: center;
}
{{/code}}

<ul>
  <li>In the above example, the CSS rule opens with a selector. This selects the HTML element that we are going to style. In this case, we are styling level one headings (<code>h1</code>).</li>
  <li>We then have a set of curly braces <code>{ }</code>. This is the declaration block</li>
  <li>Inside the braces will be one or more <strong>declarations</strong>, which take the form of <strong>property</strong> and <strong>value</strong> pairs. We specify the property (<code>color</code> in the above example) before the colon, and we specify the value of the property after the colon (<code>red</code> in this example).</li>
  <li>This example contains two declarations, one for <code>color</code> and the other for <code>text-align</code>. Each pair specifies a property of the element(s) we are selecting (<code>h1</code> in this case), then a value that we'd like to give to the property.</li>
</ul>

<p>
  Let us create a simple stylesheet first named <strong>style.css</strong> with the above example. We will create it in the same folder you placed the <strong>index.html</strong> file to.
</p>

<p>Now that we have a simple css file, add it inside the <code>head</code> tag witin the <strong>index.html</strong> file.</p>
{{#code lang="html"}}<link rel="stylesheet" href="./style.css">{{/code}}

<p>You will notice that the <code>href</code> is pointing to {{#inline-code}}./style.css{{/inline-code}}. The <code><q>.</q></code> here is to tell that the path is <strong>relative to the index.html</strong> file, and you will probably never see it on a real website because there are better ways to do this when hosting it. I will talk about that later in this blog, let us focus on the style for now.</p>

<p>Your <strong>index.html</strong> file should now look like this.</p>
{{#code lang="html"}}
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <title>A simple HTML document</title>
    <link rel="stylesheet" href="./style.css">
  </head>
  <body>
    <h1>Welcome to my simple HTML page</h1>
    <p>
      An HTML page is used for sharing information with others. And to ensure that <em>everyone</em> can access the page, we must be conscious of the <strong>semantic</strong> and <strong>accessibility</strong> when writing it.
    </p>
  </body>
</html>
{{/code}}

<p>Once you save those files and refresh your browser, the content remains the same, but you will see new the style like below.</p>

<figure class="figure">
  <picture>
    <source class="img" srcset="/images{{page.url}}/index-html-red-title.webp" width="1122" height="347" type="image/webp">
    <img class="img" src="/images{{page.url}}/index-html-red-title.png" width="1122" height="347" alt="Firefox screenshot" loading="lazy">
  </picture>
  <figcaption class="image-caption">
    <p>The heading <q>Welcome to my simple HTML page</q> is now rendered as a big and heavy text in red. It is aligned to the center of the page.</p>
  </figcaption>
</figure>

<h4 id="selectors">Understanding selectors</h4>
<p>
  To identify which element to style, we use <code>CSS selectors</code>. This can be its <code>tag name</code>, <code>id</code>, <code>class</code>, or any other <code>attributes</code>.
</p>

<p>It is also possible to style an element based on <code>user action</code> like when a user <code>hover</code> over an element, or is curruntly <code>focusing</code> on it with a mouse or keyboard.</p>

<p>The possiblities are endless, so to make a CSS stylesheet easy to read, and to reduce the burden on the browser which calculates how to style things, we should use simple selectors while also avoiding style conflicts.</p>

<p>I am going to provide some examples of selectors here, it might feel a bit overwhelming, but just like HTML elements, you do not need to understand them all. You will gradually learn as you work with them.</p>

<p>We have already seen how to style using an element's tag name, we just write it's tag name, i.e., to style <code>&lt;h1&gt;</code> element, we just write <code>h1</code> as its selector.</p>
{{#code lang='css'}}
/*Styling all h1 elements */
h1 {
  /* some styles */
}
{{/code}}

<p>The text written between <code>/*  </code>&nbsp;and&nbsp;<code>  */</code> are not considered styles, they are the way to do comments in CSS, and the browser will ignore it.</p>

<p>To style using an element's attribute, we use an attribute selector, a simple attribute selector can be written as <code>[attribute-name="value"]</code>.</p>
{{#code lang='css'}}
/* Styling all elements with the title attribute set as "cheese" */
[title="cheese"] {
  /* some styles */
}
{{/code}}


<p>Two HTML attributes, <code>id</code>, and <code>class</code> are special, because they are used a lot for styling HTML.<br>
  To style using a <code>class</code>, we prefix the class name with a <q>.</q>, so the syntax will be <code>.class-name</code>.<br>
  To style using an element's <code>id</code>, we prefix the class name with a <q>#</q>, so the syntax will be <code>#element-id</code>. 
</p>

{{#code lang='css'}}
/* Styling all elements with the class "cat" */
.cat {
  /* some styles */
}

/* Styling element with the id "mouse" */
#mouse {
  /* some styles */
}
{{/code}}

<p>It is also possible to join multiple selectors together, to do that, we just write them together <strong><em>without any space between them</em></strong>.</p>
{{#code lang='css'}}
/* Styling all image elements with the class "cat" */
image.cat {
  /* some styles */
}
{{/code}}

<p>If you put a space between selectors, that means that the selector preceeding is a child element. for example, the selector <code>#house .mouse .cheese</code> will style the element with the class <code>cheese</code> which is under an element with the class <code>mouse</code> which is under another element with the id <code>house</code></p>

{{#code lang='css'}}
/* Styling all elements with the class "cat" inside the element with id "house" */
#house .cat {
  /* some styles */
}
{{/code}}

<p>
  Let us update the <strong>style.css</strong> file a bit, I'm setting a purple colour for the background, white text colour, removed the attention seeking red heading, and increased the text size for the paragraph.
</p>

{{#code lang="css"}}
body {
  background-color: purple;
  color: white;
  line-height: 2em;
}

h1 {
  text-align: center;
}

p {
  font-size: 1.2em;
}
{{/code}}

<p>Let us refresh the page again to see new the style.</p>
<figure class="figure">
  <picture>
    <source class="img" srcset="/images{{page.url}}/index-html-purple.webp" width="1122" height="347" type="image/webp">
    <img class="img" src="/images{{page.url}}/index-html-purple.png" width="1122" height="347" alt="Firefox screenshot with new changes" loading="lazy">
  </picture>
  <figcaption class="image-caption">
    <p>The background colour has now changed to purple, all the text, including the title is now in white color. There now a bit more gap between the lines in the paragraph, and its font size has increased sightly.</p>
  </figcaption>
</figure>

<p>Now we have a problem, the colours we set might not be readable under some light conditions, and some prefers light theme while others need a dark theme. Some set their system or the browser to automatically change the theme based on time or location. It is one of the very convenient ways to avoid eye strain.</p>

<p>Well, it is not just the visitors, you might also prefer one theme over the other most of the time. While many debate about what is good for the eyes even now, switching between dark and light themes is an accessibility feature, and we are considerate to people with needs, right?</p>

<p>Most modern browsers now support a few convenient ways to tackle this in pure CSS.</p>
<dl>
  <dt><strong>Media queries</strong></dt>
  <dd>Media queries allow you to apply CSS styles depending on a device's media type (such as print vs. screen) or other features or characteristics such as screen resolution or orientation, aspect ratio, browser viewport width or height, or user preferences. These may include preferences such as preferring reduced motion, data usage, or transparency.</dd>
  <dt><strong>CSS custom properties (CSS variables)</strong></dt>
  <dd>CSS variables allow you to assign property values and dynamically change it. This is a powerful feature when combined with media queries or JavaScript (A programming language used to make websites more interactive).</dd>
  <dt><strong>color-scheme property</strong></dt>
  <dd>The color-scheme CSS property allows an element to indicate which colour schemes it can comfortably be rendered in. This is intended to help the browser style the form controls like text boxes and buttons, and scrollbars.</dd>
</dl>

<p>I will write more about Media queries and dynamic styling in the next part of this series.</p>
{{/inline}}
{{> (lookup this 'parent') }}

