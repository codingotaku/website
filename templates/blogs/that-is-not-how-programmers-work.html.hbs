{{#*inline "page"}}
<h2 id="what">What is this about?</h2>
<p>While I try to stay away from social media as much as possible, I tend to read posts and articles shared by fellow programmers, just so that I can find new and interesting things.</p>
<p>Usually, it is of mediocre stuff, but occasionally, I get some gems like <a href="https://sxmo.org/" rel="external" {{{settings.opener.metadata}}}>sxmo</a>, but I'm not going to write about a cool project this time, but instead, I want to rant about posts that are demeaning programmers. It is almost always a harmless and funny post that's taken too seriously by management or higher level people.</p>

<p>So what am I talking about? There is an old <q>meme</q> that keeps cropping around on the internet, it's a picture of a pipe built around a rock, with often with the title <q>They pay me to build pipes, not to move rocks</q>.</p>

<figure class="figure" id="figure1">
  <picture>
    <source class="img" srcset="/images{{page.url}}/pipe-built-around-rock.webp" width="800" height="769" type="image/webp">
    <img class="img" src="/images{{page.url}}/pipe-built-around-rock.jpg" width="800" height="769" alt="">
  </picture>
  <figcaption class="image-caption">
    <strong>Figure 1:</strong>
    <p>Picture of a pipe built around a rock, a small portion of the ground next to the rock is clean without any sediments. This hints that the rock was moved to fit within the bounds of the pipe.</p>
  </figcaption>
</figure>

<p>What is wrong with this picture? Well, not much if you understand it's just a funny prank. But it can be related to by many professions, including programmers, especially when a legacy program needs maintenance, so this get's circulated in almost every profession.</p>

<h2 id="the-reality">The reality</h2>

<p>I am going to nitpick on just the tech community postings as that's what I work with, but I guess what I am saying here is true almost everywhere.</p>

<p>The image is though provoking, and reminds me of times I left a complex piece of code untouched due to time constraints. Occasionally, my refactor would not get merged as the team didn't have enough time to ensure that the logic was the same due to deadlines.</p>

<p>Sadly, this has happened in all the companies that I have worked for. But in both scenarios, we create technical-debts, which does get addressed when we find time to do things, unlike the <q>technical-debt black hole</q> you read in other articles.</p>

<p>What I do not like is when the image goes from <q>haha, funny!</q> to <q>this is because developers don't do X, Y, and Z, we need to reform how things are done by devs</q>. The thing about software development is that, the way we program changes over time, so the older a project is, the messier the code is.</p>

<p>Another type of complex and messy code you encounter is when it is written by a young graduate, or by a person new to the language or framework. We should be fine with these too, as it is a necessary step to get better later.</p>

<h2 id="how-programmers-work">How do the programmers work?</h2>

<p>When we programmers see a code that we don't understand, it is in our nature to try to make sense of it, and rewrite it to be cleaner and readable. But that doesn't always happen, sometimes a block of code just works, and we would rather not touch it. This is more true when the code in question is a business critical core logic that has been working for many years without any problem.</p>

<p>However, that doesn't stop us from documenting. If a block of code is confusing, we would at least add some comments above it to make sense of it, or add a note to refactor it in the future.</p>

<p class="note">The real reason why a confusing code block is not rewritten is almost always out of fear of the time constraints, and not because refactoring is not in the scope. So if you want to blame <em>someone</em>, the blame should lie on the business that forced the programmers to go against their nature.</p>

<p>An exception to my above statement is when the person working on that code now is unfamiliar with the program, they could be new to the language/framework, or they just took ownership of that program/project.</p>

<p>Once a complex program is understood, and we have found some time to refactor it, we <strong>will</strong> refactor it. It may not be perfect, but we will try our best.</p>

<figure class="figure" id="figure2">
  <picture>
    <source class="img" srcset="/images{{page.url}}/pipe-built-without-rock.webp" width="800" height="769" type="image/webp">
    <img class="img" src="/images{{page.url}}/pipe-built-without-rock.jpg" width="800" height="769" alt="" loading="lazy">
  </picture>
  <figcaption class="image-caption">
    <strong>Figure 2:</strong>
    <p>The picture edited to show a straight pipe built without the rock. The ground is clean and does not look like there was a rock there before.</p>
  </figcaption>
</figure>

<p>The picture above was edited by me using <abbr title="GNU Image Manipulation Program">GIMP</abbr>, it took me some time to do it. As I am not an artist, it is not perfect (you can see smudges everywhere on the pipe and the surroundings), but I guess my point is clear, programmers will not do workarounds unless doing things straight way take too long.</p>

<h2 id="what-to-do">What should we do?</h2>
<p>Do we really want to work this way? Pushing the deadlines out is becoming a norm. Even with the modern tools and languages, the amount of time we wait for things to build and test functionalities has not changed, in fact, most of the <q>cloud</q> environments just make it slower. As long as we continue to do <a rel="external" {{{settings.opener.metadata}}} href="https://www.wearedevelopers.com/magazine/resume-driven-development">resume-driven development</a>, this will never end.</p>

<p>These are some things the business can do to fix this behaviour, it's mostly to stop following what other business is doing.</p>
<ul>
  <li>Hire people for their talent instead of the framework experience.</li>
  <li>Listen to the developers and give them:
    <ul>
      <li>More time to clean up the technical-debts.</li>
      <li>More time to optimize existing tools for speed and resource usage.</li>
      <li>Good work-life balance so they can work happily.</li>
    </ul>
  </li>
  <li>Listen to the customers (unless the customer is a business, in that case, listen to the developers and convince the business).</li>
  <li>Take more time to create a quality product instead of giving empty and exaggerated promises to the customers.</li>
</ul>

<p>We have reached a point in the computing where the hardware works at unimaginable efficiency, but the software and tools are getting slower and bloated.</p>

<p>It is time for a change, time for optimizations, time for bug-free software, and it is time for the users to enjoy what the hardware can do for them.</p>
<ul>
  <li>Let us do <strong>true native app development</strong> instead of the slow and bloated JavaScript and web wrappers or running them in Virtual machines.</li>
  <li>Let us create more native cross-platform frameworks that work for everyone!</li>
  <li>Let us make software with accessibility in mind for everyone to enjoy.</li>
  <li>Let us make more programs that are <a href="https://appdevelopermagazine.com/creating-apps-that-work-without-the-internet/" rel="external" {{{settings.opener.metadata}}}>not tied to servers</a> so they can be used everywhere!</li>
  <li>Let us make our programs have <a href="https://guix.gnu.org/en/blog/2015/reproducible-builds-a-means-to-an-end/" rel="external" {{{settings.opener.metadata}}}>reproducible builds</a> so we can port and work on them easily.</li>
  <li>Let us make our programs <a href="https://www.fsf.org/about/what-is-free-software" rel="external" {{{settings.opener.metadata}}}>free-software</a>.</li>
</ul>

<p>What I wrote above would seem like a software utopia, but it is achievable with the current tools, we just need to make them mainstream.</p>

<figure class="figure" id="figure3">
  <picture>
    <source class="img" srcset="/images{{page.url}}/pipe-and-rock-before-after.webp" width="1600" height="769" type="image/webp">
    <img class="img" src="/images{{page.url}}/pipe-and-rock-before-after.jpg" width="1600" height="769" alt="" loading="lazy">
  </picture>
  <figcaption class="image-caption">
    <strong>Figure 3:</strong>
    <p>The <a href="#figure1">figure 1</a> and <a href="#figure2">figure 2</a> merged into one picture with before and after sections.<br>
      <em>Before software utopia</em>: Pipe built around the rock.<br>
      <em>After software utopia</em>: Pipe built without the rock.
    </p>
  </figcaption>
</figure>

{{/inline}}
{{> (lookup this 'parent') }}

