{{#*inline "page"}}
<p class="note"><strong>Note:</strong> This post is a <strong>draft</strong> and also a living document. I may add or remove content at any time and could have some type errors, so I recommend archiving it if you are interested.</p>
<p>The most amazing thing about the UNIX system, in my opinion, is the use of <a href="https://en.wikipedia.org/wiki/Pipeline_(Unix)" rel="external" {{{settings.opener.metadata}}}>UNIX pipeline</a> to combine or chain programs. Some of the scripts in my <a href="/projects/fuzzel-scripts">fuzzel scripts</a> project would've been a lot more complicated without it.</p>

<p>Adding the ability to interact with a program via other programs will make the developer&apos;s life easier by reducing the features they need to implement.</p>

<p>While the UNIX pipeline is arguably the most modular system in computers, this post is not about that. I want to talk about a few projects that allows the end users to essentially <q>hack</q> the program to improve their workflow, or even have some fun!</p>

<h2 id="my-workflow">My Workflow</h2>
<p>I am a programmer, so my eyes are glued to a computer screen most of the time as my work, hobby, and entertainment revolves around it. While many things in on this Earth are now run using computers, most people do not use it for as long as I do.</p>

<p>This means that I want to modify the computer I use a lot so that I don't feel hindered with it. If I want to do something regularly, I make sure that the amount of time I spend on it is reduced as much as possible.</p>

<h2 id="entertainment">Entertainment</h2>
<p>I am starting with entertainment as this is what most people use a computer for. My entertainment with computer involves reading articles, playing games, and watching videos — in that order.</p>

<h3 id="articles">Modular Article Reading</h3>
<p>I usually read articles through a <a href="/feeds">feed/news reader program</a>. Sometimes, the websites will not give me the complete text content in their feed. While this is a bit annoying, most of the readers are smart enough to download the complete content from the source website. But what about the links within the article?</p>

<p>While I avoid adding websites to my feed that have <a rel="external" {{{settings.opener.metadata}}} href="https://mobiforge.com/research-analysis/the-web-is-doom">page size larger than doom</a>, the articles I read may still have links to them, and I may want to open that link for better reference.</p>

<p>I read the feeds using <a href="https://newsboat.org/" rel="external" {{{settings.opener.metadata}}}>Newsboat</a>, it is a wonderful tool. And its built-in browser can handle opening most links in the feed, but it has trouble with some websites, because of this, I swap the in-built browser with my preferred terminal browser <a href="http://links.twibright.com/" rel="external" {{{settings.opener.metadata}}}>links</a>.</p>

<p> This can be done by editing the {{#inline-code}}~/.config/newsboat/config{{/inline-code}} file:</p>
{{#code}}browser "links %u" # here %u stands for URL{{/code}}

<p>But just changing the browser is not making a tool modular, Newsboat has the capability to define <a href="https://newsboat.org/releases/2.32/docs/newsboat.html#_macro_support" rel="external" {{{settings.opener.metadata}}}>custom macros</a>, and the <a href="https://newsboat.org/releases/2.37/docs/newsboat.html#_switching_browser_for_different_tasks" rel="external" {{{settings.opener.metadata}}}>set browser</a> command can be used to run arbitrary commands in the shell. <em>This</em> is what make Newsboat a powerful tool.</p>

<p>The macros are typically stored to a key, usually a letter or a number is used for this purpose, but could be any key. To avoid Newsboat confusing it with built-in key binding, the macros are triggered by pressing the <q><kbd>,</kbd></q> key first followed by the macro key.</p>
<p>For example, to trigger a macro stored to the letter <q><kbd>m</kbd></q>, we will need to type <q><kbd>,m</kbd></q></p>
<p>And of course, <a href="https://newsboat.org/releases/2.37/docs/newsboat.html#_macro_support" rel="external" {{{settings.opener.metadata}}}>the macro key can also be changed to something else</a> if you want to!</p>

<h4 id="article-links">Handling Troublesome Links</h4>
<p>Some websites out there are unreadable with my terminal browser, like the ones that dynamically loads the text content or have unnecessary content that is not part of the article I am reading.</p>
<p>Lucky for us, my GUI browser of choice, <a href="https://librewolf.net/" rel="external" {{{settings.opener.metadata}}}>Librewolf</a> (Firefox-based), can open a link in the <a href="https://support.mozilla.org/en-US/kb/firefox-reader-view-clutter-free-web-pages" rel="external" {{{settings.opener.metadata}}}>Reader View</a> by prefixing the text {{#inline-code}}about:reader?url={{/inline-code}} to the URL. For example, if you go to {{#inline-code}}about:reader?url=http://codingotaku.com/blogs/modular-programs{{/inline-code}} in any Firefox-based browser, you will be able to read this page in the reader view.</p>

<p>So I created a macro that will change my browser to Librewolf's reader view, opens the URL, and change the browser back to the links browser:</p>
{{#code}}macro o set browser "librewolf about:reader?url=%u"; open-in-browser; set browser "links %u"{{/code}}
<p>Now all I need to do is type <q><kbd>,o</kbd></q> to see the article without clutter! Cool right?</p>

<h4 id="article-images">Viewing Images</h4>
<p>As I usually do not subscribe to feeds without text as the main content, it&apos;s rare for me to view images in it, this is one of the reasons why I chose Newsboat instead of other fancy tools.</p>
<p>But, I sometimes want to open images in the feed using my system&apos;s image viewer <strong>swayimg</strong> so I can zoom, copy, or even edit the image later. <strong>swayimg</strong> can run arbitrary commands just like Newsboat!</p>
<p>So I created a macro that does the following, in the order:</p>
<ol>
  <li>Set the default browser as <strong>swayimg</strong>.</li>
  <li>Run a <code>curl</code> command using the <strong>swayimg</strong>&apos;s custom <code>exec</code> protocol to download the image and display it.</li>
  <li>After the image is displayed, I update the default browser back to Librewolf.</li>
</ol>
<p>The Newsboat macro looks like the following:</p>
{{#code}}macro i set browser "swayimg 'exec://curl -s %u'"; \
  open-in-browser; \
  set browser "links %u"{{/code}}
<p>It might look complicated, but it's not much different than what a generic browser would do. The benefits are the following:</p>
<ul>
  <li>I don't download huge media unless I <em>really</em> want to do so.</li>
  <li>As <strong>swayimg</strong> can also run custom commands, I can do more things with the image like sharing or editing it with a few keypress.</li>
</ul>

<h4 id="article-media">Watching Videos or Listening to Music</h4>
<p>It is very rare for me to open a video link from a feed, but when I do, I try opening it using my video player <strong>mpv</strong> first. <strong>mpv</strong> can use youtube-dl to stream videos, and contrary to the name, youtube-dl can handle many websites other than YouTube.</p>
<p>Doing this is just as easy as replacing <strong>swayimg</strong> with <strong>mpv</strong> in my previous macro:</p>
{{#code}}macro m set browser "mpv %u"; open-in-browser; set browser "links %u"{{/code}}
<p>The same macro will work with music too, so I use the key <q><kbd>m</kbd></q> for <q>media</q></p>

<h4 id="article-mentions">Sending Myself Webmentions</h4>
<p>If a feed I follow mentions my website, and they support IndieWeb, I can send myself a <a href="https://www.w3.org/TR/webmention/" rel="external" {{{ settings.opener.metadata }}}>Webmention</a> for that article. This can also be done by simple browser switching:</p>
{{#code}}macro w set browser "curl -si https://codingotaku.com/webmention -d source=%u -d target=https://codingotaku.com"; \
  open-in-browser; \
  set browser "links %u"
{{/code}}
<p>This is not perfect as this needs additional info about which page is being mentioned, but I can do that from my website after receiving the mention, so this is good enough for me.</p>
<h4 id="article-protocols">Opening Other Custom Protocols</h4>
<p>If there are other types of links, such as <code>mailto:</code>, <code>matrix:</code>, etc., which are meant to open other specialized clients, I can do it using the <code>links</code> browser, but not for all protocols. So I set a macro that opens links using <code>xdg-open</code>, which can handle all types of links that are registered in my system.</p>
{{#code}}macro d set browser "xdg-open %u"; open-in-browser; set browser "links %u"{{/code}}
<p>I never had to use it, but it's a nice macro to have.</p>
<h4 id="why-not-a-browser">Why Not Just Use a Browser?</h4>
<p>While reading this, you might have thought to yourself, why not just use a normal browser? Or a web-based feed reader which can do all these?</p>
<p>My Ideal feed reader should be able to do all these things and should have the capability to extend more. It doesn't need to be a terminal-based one like Newsboat, but I want it to also be lightweight, configurable, and work on my <a href="/blogs/switching-to-gentoo#building-a-pure-wayland-system">pure Wayland system</a>. Everyone has their on criteria, and this is mine, and once configured, <q>it just works</q>.</p>

<h3 id="videos">Modular Video Watching</h3>
<p>If you thought Newsboat is overly configurable, you are in for a treat! <a href="https://mpv.io/" rel="external" {{{settings.opener.metadata}}}>mpv</a> is a program for watching videos, while it is keyboard-centric, there are some <a href="https://github.com/mpv-player/mpv/wiki/Applications-using-mpv#gui-frontends" rel="external nofollow" {{{ settings.opener.metadata }}}>GUI frontends</a> which make it easy to use without a keyboard. Anyway, <strong>mpv</strong> is heavily scriptable, and it can be scripted and extended using <a href="https://github.com/mpv-player/mpv/wiki/User-Scripts#lua-scripts" rel="external nofollow" {{{ settings.opener.metadata }}}>lua</a>, <a href="https://github.com/mpv-player/mpv/wiki/User-Scripts#javascript" rel="external nofollow" {{{ settings.opener.metadata }}}>javascript</a>, <a href="https://github.com/mpv-player/mpv/wiki/User-Scripts#user-shaders" rel="external nofollow" {{{ settings.opener.metadata }}}>VapourSynth</a>, and <a href="https://github.com/mpv-player/mpv/wiki/User-Scripts#c-plugins" rel="external nofollow" {{{ settings.opener.metadata }}}>even with C</a>!</p>

<p>The community have created a wide range of scripts, it's just too long to list here, you can find most of them at the <a href="https://github.com/stax76/awesome-mpv" rel="external nofollow" {{{ settings.opener.metadata }}}>Awesome mpv repository</a>. Some of the most popular ones are:</p>
<ul>
  <li><a href="https://github.com/Eisa01/mpv-scripts/#smartskip" rel="external nofollow" {{{ settings.opener.metadata }}}>SmartSkip</a> - Skip opening, intro, outro, preview. Jump to next and previous files, and save chapter changes!</li>
  <li><a href="https://github.com/marzzzello/mpv_thumbnail_script" rel="external nofollow" {{{ settings.opener.metadata }}}>mpv thumbnail script</a> - show thumbnails superfast on hover.</li>
  <li><a href="https://github.com/po5/mpv_sponsorblock" rel="external nofollow" {{{ settings.opener.metadata }}}>mpv sponsorblock</a> - skip sponsors in YouTube videos.</li>
  <li><a href="https://github.com/b1scoito/mpv-cut" rel="external nofollow" {{{ settings.opener.metadata }}}>mpv cut</a> - video cutting/clipping/slicing script.</li>
</ul>

<p><strong>mpv</strong> has support to watch streaming videos too, including YouTube! All you need is to install the <a href="https://github.com/yt-dlp/yt-dlp/" rel="external nofollow" {{{ settings.opener.metadata }}}>yt-dlp</a> program in your system.</p>

<p>Ever wanted to watch a video with your friends or family over the internet? I know I have, <strong>mpv</strong> can be integrated with <a href="https://syncplay.pl/"  rel="external nofollow" {{{ settings.opener.metadata }}}>SyncPlay</a> to do this.</p>

<p>Once you get the hang of using <strong>mpv</strong>, you will try to use it everywhere, this is the reason for the large number of active community scripts. The modular nature makes it easy to integrate <strong>mpv</strong> to almost anything. Did you know that you can use <a href="https://github.com/mpv-player/mpv/wiki/Video4Linux2-Input" rel="external nofollow" {{{ settings.opener.metadata }}}>mpv to stream from your camera?</a></p>


<h4 id="playlist">Playlists</h4>
<p>I have a list of songs,  music, and videos that I like. And I also like storing them in a <abbr><strong>TSV</strong></abbr> (Tab Seperated Value) file with the name and the URL.</p>

<p>A playlist file will look something like this:</p>
{{#code}}
URL to YouTube	Song1 name
URL to Vimeo	Song2 name
File path	Song3 name
Directory path	Collection name
{{/code}}

<p>This might look weird for many, but it works better than using any apps, because I can store URL from multiple services, and also store a file, or directory path (collection) in it.</p>

<p>The song name is just for me to know what that URL is for. It is not realy needed if all I have are file and directory paths.</p>
<p>And to play that list without video, I just need to run {{#inline-code}}cut -f1 playlist-name.tsv | mpv --video=no --playlist=-{{/inline-code}}, this will play the songs in the same order as in the file.</p>
<p>To shuffle the playlist, I just need to insert the <code>shuf</code> tool in the script: {{#inline-code}}cut -f1 playlist-name.tsv | shuf |mpv --video=no --playlist=-{{/inline-code}}.</p>

<p>I use the <code>cut</code> tool to remove the song/collection name from the file, as it serves no purpose for <code>mpv</code>.</p>
<p>But if I am not storing the names in the file, the script will be much simpler, I could just create a list of paths/URLs in a text file and run {{#inline-code}}mpv --video=no --playlist=playlist-file.txt{{/inline-code}}.</p>

<p>This doesn't mean that I type the whole command all the time, I just have a script named <code>playlist</code> which just play the songs in random order. For what it does, it could just be an <code>alias</code>, but I want to improve it in the futre, so it is what it is for the time being.</p>

<p><code>mpv</code> is a monolithic tool, but it integrates well with other other tools that are <q>unix-y</q>. I may never use another media player unless I find them more efficient than <code>mpv</code>.</p>

<h3 id="games">Modular Gaming</h3>
<p>Like all entertainments, Games have been struck by a profit-driven mentality. Whether it is developing, publishing, or just playing, it is a bit difficult to find games that are friendly to the community. This means that extensible games are even harder to come by. Even for single player offline games.</p>

<p>But that does not stop us from developing modes, there are a lot of modes out there, even for the propriatory ones, which I do not want to link here.</p>
<p>Many Foss games support scripting to extend them, and also actively promote doing it. I'll list a few:</p>
<ul>
  <li><a href="https://play0ad.com/">0.A.D</a> - A free, open-source game of ancient warfare.</li>
  <li><a href="https://instead.hugeping.ru/en/">INSTEAD</a> -  An engine designed to make the textographic games.</li>
  <li><a href="https://mindustrygame.github.io/">Mindustry</a> - A sandbox tower-defence game.</li>
  <li><a href="https://www.minetest.net/">Luanti (formerly Minetest)</a> - An open-source voxel game engine.</li>
</ul>

<p>What I like about these games and engines is the ease of modifying the game without needing to reverse engineer a bunch of stuff, everything I need is documented well.</p>
<p>But these games and engines are <em>still not as modular as the other tools</em> that I use, and makes me lose my interest in tinkering with it after a while.</p>

<p>If you know of some good games that can be integrated with other tools in the system, let me know. What I like to do is interact with the operating system from within the game and vise versa. For example:</p>
<ul>
  <li>Getting the game notifications and achievements on my system&apos;s default notification program.</li>
  <li>Ability to use the system menu (like <a href="https://codeberg.org/dnkl/fuzzel" rel="external" {{{ settings.opener.metadata }}}>fuzzel</a>) program instead of the game&apos;s menu for navigation.</li>
  <li>Adding quests to a To-Do list or a text file.</li>
  <li>Using the system&apos;s text-to-speech engine to read dialogues out loud if the game doesn't have built in voice for characters.</li>
</ul>
<p>The game itself should not do all these, but instead, it should provide an ability to interact with the game via configurable macros like what Newsboat does. Or it should have <strong>mpv</strong> like scriptability to do many more things that I have not thought of yet.</p>

<h2 id="hobbies">Hobbies</h2>
<p>As I said in the <a href="#my-workflow">My Workflow section</a>, even my hobbies live inside a computer.</p>
<p>I create scripts, tools, and customise my system in my leisure time. I find it rewarding when I make things easier for myself to work with.</p>
<p>You can find most of my tools at my <a href="https://codeberg.org/codingotaku" rel="external me" {{{ settings.opener.metadata }}}>codeberg repositories</a>. And most of my system customization in the <a href="https://codeberg.org/codingotaku/dotfiles" rel="external me" {{{ settings.opener.metadata }}}>dotfiles</a>.</p>

<h3 id="what-is-modular-about-customisation">So What is <q>Modular</q> About Configuring System?</h3>
<p>What I have is a well-integrated system that suites my taste, this is done so by connecting programs to each other. Such as <code>Fuzzel</code>, <code>Foot</code>, <code>Emacs</code>, <code>Bash</code>, <code>jq</code>, etc., Each with their own additional plugins, and each can replaced with another program without much hassle. well, may be not Emacs, but you can find replacements for all Emacs packages that I use as it&apos;s an extensible text editor.</p>

<p>I am underselling this section by not adding much here, but I have spent many years configuring my system that it is simply not possible to add all of it into a single post. Maybe I will do a series on it when I don't have anything else to share.</p>

<h2 id="work">Work</h2>
<p>At my work, many programs are restricted. Heck, I can't even access my (this) website. I am given an M2 MacBook Pro to work with, which doesn't really go hand-in-hand with my usual workflow either.</p>
<p>Besides all the restrictions, I managed to install GNU Emacs in the laptop, which comes with its own modularities. But I am yet to set it up to do anything other than programming at the moment. I will try to do more and mention add the details here when I get some time.</p>
<p>I am hoping to use Emacs to read my work emails instead of outlook, but I haven't succeeded yet. I have read that <a href="https://brettpresnell.com/post/email/" rel="external" {{{ settings.opener.metadata }}}>Brett Presnell</a> managed to do this, but either Microsoft has changed a few things, or my workplace has more restrictions than I already know.</p>

<h2 id="non-modular">Non-modular Programs Are the Worst!</h2>
<p>In my opinion, non-modular programs make things difficult over time. While one may be allured by the all-in-one programs that don't need much tinkering, the moment you need to do something that the program does not have a built-in way for, the more difficult your workflow will become.</p>

<p>While this is more true for proprietary programs, sometimes even an <q>open-source</q> program can also make things difficult by deliberately not making things configurable. For instance, the Android Open-Source Project (<abbr>AOSP</abbr>) <a href="https://issuetracker.google.com/issues/166478545" rel="external" {{{ settings.opener.metadata }}}>does not have the ability to hide the navigation pill</a>, and this makes other brands and custom ROMs do workarounds and their own patches to hide it.</p>

<p>Some VS Code extensions restrict themselves <a href="https://github.com/VSCodium/vscodium/blob/master/docs/index.md#proprietary-debugging-tools" rel="external nofollow" {{{ settings.opener.metadata }}}>to work only with the official build of VS Code</a>, crippling anyone who wants to build and use their own version of VS Code.</p>

<p>IPhone has <a href="https://discussions.apple.com/thread/250237134" rel="external nofollow" {{{ settings.opener.metadata }}}>no ability to permanently disable Bluetooth</a>. And due to its non-modular and proprietary nature, you have no way to even workaround this.</p>

{{/inline}}
{{> (lookup this 'parent') }}

