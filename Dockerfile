FROM docker.io/library/rust:1.84-slim-bullseye

USER root
RUN useradd -ms /bin/bash wemblate
RUN mkdir /website && chown wemblate website

USER wemblate
WORKDIR /website

## copy the main binary
COPY --chown=wemblate --from=builder /website/main ./

## copy runtime assets
COPY --chown=wemblate --from=builder /website/App.toml ./App.toml
COPY --chown=wemblate --from=builder /website/static ./static
COPY --chown=wemblate --from=builder /website/templates ./templates
COPY --chown=wemblate --from=builder /website/resources ./resources

## ensure the container listens globally on port 1337
ENV ROCKET_ADDRESS=0.0.0.0
ENV ROCKET_PORT=1337

CMD ["./main"]

