/*
Copyright (C) 2021-2030 Coding Otaku (Rahul Sivananda) <contact@codingotaku.com>

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU Affero General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
details.

You should have received a copy of the GNU Affero General Public License along
with this program. If not, see <https://www.gnu.org/licenses/>.
*/

use crate::utils::Card;
use rocket::serde::Serialize;

#[derive(Copy, Clone)]
pub(crate) enum Category<'e> {
    Admin,
    All,
    AllForTag(&'e str),
    Blogs,
    BlogsForTag(&'e str),
    ChaptersForStory(&'e str),
    ChaptersForTag(&'e str, &'e str),
    Main,
    MainsForTag(&'e str),
    Projects,
    ProjectsForTag(&'e str),
    Stories,
    StoriesForTag(&'e str),
}

impl Category<'_> {
    pub(crate) fn generate_list_description(&self) -> String {
        match self {
            Self::All => String::from("This page contains all posts on this website"),
            Self::AllForTag(tag) => format!("This page contains all posts on this website for {}.", tag),
            Self::Main => String::from("These are all the top-level pages that do not belong to a category."),
            Self::Admin => String::new(),
            Self::MainsForTag(tag) => format!("This page contains all main pages on  {}.", tag),
            Self::Blogs => String::from("This page contains all my Blogs, I do some level of research on these."),
            Self::BlogsForTag(tag) => format!("This page contains all my Blogs on  {}.", tag),
            Self::Projects => String::from("This page contains some of my Projects."),
            Self::ProjectsForTag(tag) => format!("This page contains my Projects on {}.", tag),
            Self::Stories => String::from("This page contains all my Stories, the updates will be slow as I am almost always busy."),
            Self::StoriesForTag(tag) => format!("This page contains all my Stories on {}.", tag),
            Self::ChaptersForStory(story) => {
                format!("This page contains all chapters for {}", story)
            }
            Self::ChaptersForTag(story, tag) => {
                format!("This page contains all chapters for {} on {}", story, tag)
            }
        }
    }

    pub(crate) fn generate_feed_title(&self, card: &Card) -> String {
        format!("{} - {}", self.get_title_case(), card.display_name)
    }

    pub(crate) fn get_type(&self) -> String {
        match self {
            Self::All | Self::AllForTag(_) => String::from("all"),
            Self::Main | Self::MainsForTag(_) => String::from("main"),
            Self::Admin => String::from("admin"),
            Category::Blogs | Category::BlogsForTag(_) => String::from("blogs"),
            Category::Projects | Category::ProjectsForTag(_) => String::from("projects"),
            Category::Stories | Category::StoriesForTag(_) => String::from("stories"),
            Category::ChaptersForStory(story) | Category::ChaptersForTag(story, _) => {
                format!("stories/{}", story)
            }
        }
    }

    pub(crate) fn get_url(&self) -> String {
        match self {
            Self::All => String::from("/all"),
            Self::AllForTag(tag) => format!("/all/tags/{}", tag),
            Self::Main => String::from("/main"),
            Self::MainsForTag(tag) => format!("/main/tags/{}", tag),
            Self::Admin => String::from("/admin"),
            Category::Blogs => String::from("/blogs"),
            Category::BlogsForTag(tag) => format!("/blogs/tags/{}", tag),
            Category::Projects => String::from("/projects"),
            Category::ProjectsForTag(tag) => format!("/projects/tags/{}", tag),
            Category::Stories => String::from("/stories"),
            Category::StoriesForTag(tag) => format!("/stories/tags/{}", tag),
            Category::ChaptersForStory(story) => format!("/stories/{}", story),
            Category::ChaptersForTag(story, tag) => format!("/stories/{}/tags/{}", story, tag),
        }
    }

    pub(crate) fn get_title_case(&self) -> String {
        match self {
            Self::All => String::from("All posts"),
            Self::AllForTag(tag) => format!("All posts on {}", tag),
            Self::Main | Self::Admin => String::from("Main pages"),
            Self::MainsForTag(tag) => format!("Main pages on {}", tag),
            Category::Blogs => String::from("Blogs"),
            Category::BlogsForTag(tag) => format!("Blogs on {}", tag),
            Category::Projects => String::from("Projects"),
            Category::ProjectsForTag(tag) => format!("Projects on {}", tag),
            Category::Stories => String::from("Stories"),
            Category::StoriesForTag(tag) => format!("Stories on {}", tag),
            Category::ChaptersForStory(story) => format!("Chapters for {}", story),
            Category::ChaptersForTag(story, tag) => {
                format!("Chapters for {} on {}", story, tag)
            }
        }
    }
}

pub(crate) enum FeedType {
    Atom,
    Json,
}

#[derive(Serialize)]
#[serde(crate = "rocket::serde")]
pub(crate) struct FeedElement<'f> {
    pub url: String,
    pub name: String,
    pub mimetype: &'f str,
}

impl<'f> FeedElement<'f> {
    fn using(feed_type: FeedType, category: Category, card: &Card) -> FeedElement<'f> {
        FeedElement {
            url: feed_type.get_url(category, card),
            mimetype: match feed_type {
                FeedType::Atom => "application/atom+xml",
                FeedType::Json => "application/json",
            },
            name: format!("All {} - {}", category.get_title_case(), feed_type),
        }
    }
}

impl std::fmt::Display for FeedType {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            match self {
                FeedType::Atom => "Atom",
                FeedType::Json => "JSON",
            }
        )
    }
}

impl FeedType {
    fn get_feed_file_name(&self) -> &str {
        match self {
            FeedType::Atom => "atom.xml",
            FeedType::Json => "feed.json",
        }
    }

    pub(crate) fn get_all_main_urls(card: &Card) -> Vec<FeedElement<'_>> {
        Vec::<FeedElement>::from([
            FeedElement::using(FeedType::Atom, Category::Blogs, card),
            FeedElement::using(FeedType::Atom, Category::Projects, card),
            FeedElement::using(FeedType::Atom, Category::Stories, card),
            FeedElement::using(FeedType::Json, Category::Blogs, card),
            FeedElement::using(FeedType::Json, Category::Projects, card),
            FeedElement::using(FeedType::Json, Category::Stories, card),
        ])
    }

    pub(crate) fn get_all_category_urls<'f>(
        category: Category,
        card: &'f Card,
    ) -> Vec<FeedElement<'f>> {
        Vec::<FeedElement>::from([
            FeedElement::using(FeedType::Atom, category, card),
            FeedElement::using(FeedType::Json, category, card),
        ])
    }

    pub(crate) fn get_url(&self, category: Category, card: &Card) -> String {
        match category {
            Category::All | Category::AllForTag(_) | Category::Admin => String::new(),
            _ => format!(
                "{}{}/{}",
                card.homepage,
                category.get_url(),
                self.get_feed_file_name()
            ),
        }
    }
}
