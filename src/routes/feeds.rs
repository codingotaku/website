/*
Copyright (C) 2021-2030 Coding Otaku (Rahul Sivananda) <contact@codingotaku.com>

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU Affero General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
details.

You should have received a copy of the GNU Affero General Public License along
with this program. If not, see <https://www.gnu.org/licenses/>.
*/

use crate::metadata::{Category, FeedType};
use crate::modals::feed::{Authors, Feed, FeedItem};
use crate::modals::page::{Page, Pages};
use crate::utils::{AppConfig, Card};
use rocket::futures::future;

use rocket::serde::json::Json;
use rocket::{get, routes, Route, State};

use rocket_dyn_templates::{context, Metadata, Template};

fn pages_to_json_feed<'f>(
    pages: Pages,
    category: Category,
    feed_url: String,
    metadata: Metadata,
    card: &'f Card,
) -> Feed<'f> {
    let items: Vec<FeedItem> = pages
        .items
        .into_iter()
        .map(|page| {
            let page_url = format!("{}/{}", card.homepage, page.url);
            let banner_image = page.banner.as_ref().map_or(
                format!("{}/images/{}", card.homepage, "default-banner.webp"),
                |banner| format!("{}/images/{}/{}", card.homepage, page.url, banner.file),
            );

            let template = if page.url.eq("/") { "/home" } else { &page.url }.replacen('/', "", 1);

            let content_html =
                match category {
                    Category::Stories | Category::StoriesForTag(_) => String::new(),
                    _ => metadata
                        .render(
                            template,
                            context! {parent: "feeds/blank", url: &page.url, banner: page.banner},
                        )
                        .unwrap()
                        .1,
                };

            FeedItem {
                id: page.url,
                summary: page.desc,
                content_html,
                title: page.title,
                date_published: page.date,
                banner_image,
                date_modified: page.last_updated,
                tags: page.tags,
                external_url: page.ext_url,
                url: page_url,
            }
        })
        .collect();

    Feed {
        version: "https://jsonfeed.org/version/1.1",
        home_page_url: &card.homepage,
        feed_url,
        title: category.generate_feed_title(card),
        items,
        language: "en-gb-oxendict",
        icon: format!("{}/images/me.svg", card.homepage),
        authors: Vec::from([Authors {
            name: &card.display_name,
            url: &card.homepage,
            avatar: format!("{}/images/me.svg", card.homepage),
        }]),
    }
}

async fn render_json_feed<'f>(
    category: Category<'_>,
    metadata: Metadata<'_>,
    card: &'f Card,
) -> Json<Feed<'f>> {
    let pages = Pages::new(category).await;
    let feed_url = FeedType::Json.get_url(category, card);
    Json(pages_to_json_feed(
        pages, category, feed_url, metadata, card,
    ))
}

async fn render_atom_feed(category: Category<'_>, card: &Card) -> Template {
    let pages = Pages::new(category).await;
    let feed_url = FeedType::Atom.get_url(category, card);
    let category_type = category.get_type();
    let items = &pages.items;

    if items.is_empty() {
        Template::render(
            "feeds/atom-empty",
            context! {
              title: category.generate_feed_title(card),
              pages: items,
              meta: card,
              cat: category_type,
              feed_url
            },
        )
    } else {
        Template::render(
            "feeds/atom",
            context! {
              updated: items[0].last_modified(),
              title: category.generate_feed_title(card),
              pages: &items,
              meta: card,
              cat: category_type,
              feed_url,
            },
        )
    }
}

#[get("/blogs/atom.xml")]
async fn blogs_atom(app_config: &State<AppConfig>) -> Template {
    render_atom_feed(Category::Blogs, &app_config.card).await
}

#[get("/blogs/feed.json")]
async fn blogs_json<'f>(
    app_config: &'f State<AppConfig>,
    metadata: Metadata<'f>,
) -> Json<Feed<'f>> {
    render_json_feed(Category::Blogs, metadata, &app_config.card).await
}

#[get("/blogs/tags/<tag>/atom.xml")]
async fn blogs_tag_atom(app_config: &State<AppConfig>, tag: String) -> Template {
    render_atom_feed(Category::BlogsForTag(&tag), &app_config.card).await
}

#[get("/blogs/tags/<tag>/feed.json")]
async fn blogs_tag_json<'f>(
    app_config: &'f State<AppConfig>,
    metadata: Metadata<'f>,
    tag: String,
) -> Json<Feed<'f>> {
    render_json_feed(Category::BlogsForTag(&tag), metadata, &app_config.card).await
}

#[get("/projects/atom.xml")]
async fn projects_atom(app_config: &State<AppConfig>) -> Template {
    render_atom_feed(Category::Projects, &app_config.card).await
}

#[get("/projects/feed.json")]
async fn projects_json<'f>(
    app_config: &'f State<AppConfig>,
    metadata: Metadata<'f>,
) -> Json<Feed<'f>> {
    render_json_feed(Category::Projects, metadata, &app_config.card).await
}

#[get("/projects/tags/<tag>/atom.xml")]
async fn projects_tag_atom(app_config: &State<AppConfig>, tag: String) -> Template {
    render_atom_feed(Category::ProjectsForTag(&tag), &app_config.card).await
}

#[get("/projects/tags/<tag>/feed.json")]
async fn projects_tag_json<'f>(
    app_config: &'f State<AppConfig>,
    metadata: Metadata<'f>,
    tag: String,
) -> Json<Feed<'f>> {
    render_json_feed(Category::ProjectsForTag(&tag), metadata, &app_config.card).await
}

#[get("/main/atom.xml")]
async fn mains_atom(app_config: &State<AppConfig>) -> Template {
    render_atom_feed(Category::Main, &app_config.card).await
}

#[get("/main/feed.json")]
async fn mains_json<'f>(
    app_config: &'f State<AppConfig>,
    metadata: Metadata<'f>,
) -> Json<Feed<'f>> {
    render_json_feed(Category::Main, metadata, &app_config.card).await
}

#[get("/main/tags/<tag>/atom.xml")]
async fn mains_tag_atom(app_config: &State<AppConfig>, tag: String) -> Template {
    render_atom_feed(Category::MainsForTag(&tag), &app_config.card).await
}

#[get("/main/tags/<tag>/feed.json")]
async fn mains_tag_json<'f>(
    app_config: &'f State<AppConfig>,
    metadata: Metadata<'f>,
    tag: String,
) -> Json<Feed<'f>> {
    render_json_feed(Category::MainsForTag(&tag), metadata, &app_config.card).await
}

#[get("/stories/atom.xml")]
async fn stories_atom(app_config: &State<AppConfig>) -> Template {
    render_atom_feed(Category::Stories, &app_config.card).await
}

#[get("/stories/feed.json")]
async fn stories_json<'f>(
    app_config: &'f State<AppConfig>,
    metadata: Metadata<'f>,
) -> Json<Feed<'f>> {
    render_json_feed(Category::Stories, metadata, &app_config.card).await
}

#[get("/stories/tags/<tag>/atom.xml", rank = 2)]
async fn stories_tag_atom(app_config: &State<AppConfig>, tag: String) -> Template {
    render_atom_feed(Category::StoriesForTag(&tag), &app_config.card).await
}

#[get("/stories/tags/<tag>/feed.json", rank = 2)]
async fn stories_tag_json<'f>(
    app_config: &'f State<AppConfig>,
    tag: String,
    metadata: Metadata<'f>,
) -> Json<Feed<'f>> {
    render_json_feed(Category::StoriesForTag(&tag), metadata, &app_config.card).await
}

#[get("/stories/<story>/atom.xml", rank = 2)]
async fn story_atom(app_config: &State<AppConfig>, story: String) -> Template {
    render_atom_feed(Category::ChaptersForStory(&story), &app_config.card).await
}

#[get("/stories/<story>/feed.json", rank = 2)]
async fn story_json<'f>(
    app_config: &'f State<AppConfig>,
    story: String,
    metadata: Metadata<'f>,
) -> Json<Feed<'f>> {
    render_json_feed(
        Category::ChaptersForStory(&story),
        metadata,
        &app_config.card,
    )
    .await
}

#[get("/stories/<story>/tags/<tag>/feed.json", rank = 2)]
async fn story_tag_json<'f>(
    app_config: &'f State<AppConfig>,
    story: String,
    tag: String,
    metadata: Metadata<'f>,
) -> Json<Feed<'f>> {
    render_json_feed(
        Category::ChaptersForTag(&story, &tag),
        metadata,
        &app_config.card,
    )
    .await
}

#[get("/stories/<story>/tags/<tag>/atom.xml", rank = 2)]
async fn story_tag_atom(app_config: &State<AppConfig>, story: String, tag: String) -> Template {
    render_atom_feed(Category::ChaptersForTag(&story, &tag), &app_config.card).await
}

#[get("/sitemap.xml")]
async fn sitemap(app_config: &State<AppConfig>) -> Template {
    let (mains_future, blogs_future, projects_future, stories_future) = future::join4(
        Pages::new(Category::Main),
        Pages::new(Category::Blogs),
        Pages::new(Category::Projects),
        Pages::new(Category::Stories),
    )
    .await;

    let mains = mains_future.items;
    let blogs = blogs_future.items;
    let projects = projects_future.items;
    let stories = &stories_future.items;
    let all_chapters_futres: Vec<_> = stories
        .iter()
        .map(|story| {
            let title = story.url.split('/').last().unwrap();
            Pages::new(Category::ChaptersForStory(title))
        })
        .collect();

    let chapters = future::join_all(all_chapters_futres)
        .await
        .into_iter()
        .flat_map(|chapter_vec| chapter_vec.items)
        .collect::<Vec<Page>>();

    Template::render(
        "feeds/sitemap",
        context! {mains, blogs, projects, stories, chapters, meta: &app_config.card},
    )
}

pub(in crate::routes) fn all_routes() -> Vec<Route> {
    routes![
        blogs_atom,
        blogs_tag_atom,
        blogs_json,
        blogs_tag_json,
        mains_atom,
        mains_json,
        mains_tag_atom,
        mains_tag_json,
        projects_atom,
        projects_json,
        projects_tag_atom,
        projects_tag_json,
        stories_atom,
        stories_json,
        story_atom,
        story_json,
        story_tag_atom,
        story_tag_json,
        stories_tag_json,
        stories_tag_atom,
        sitemap
    ]
}
