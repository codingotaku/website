/*
Copyright (C) 2021-2030 Coding Otaku (Rahul Sivananda) <contact@codingotaku.com>

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU Affero General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
details.

You should have received a copy of the GNU Affero General Public License along
with this program. If not, see <https://www.gnu.org/licenses/>.
*/

use crate::db::{add_auth_code, delete_auth_code, get_auth_code, Db};
use crate::modals::indie_auth::{
    IndieAuthCode, IndieAuthCodeVerification, IndieAuthForm, IndieAuthResponse,
};
use crate::modals::settings::Settings;
use crate::utils::constants::PAGE_TEMPLATE;
use crate::utils::AppConfig;
use rocket::form::Form;
use rocket::http::{uri, uri::Authority, Status};
use rocket::response::Redirect;
use rocket::serde::json::Json;
use rocket::time::format_description::well_known;
use rocket::time::{Duration, OffsetDateTime};
use rocket::{get, post, routes, Route, State};
use rocket_db_pools::Connection;
use rocket_dyn_templates::{context, Template};
use uuid::Uuid;

fn is_port_matching(source: Option<u16>, target: Option<u16>) -> bool {
    (source.is_none() && target.is_none())
        || (source.is_some_and(|_| target.is_some()) && source.unwrap().eq(&target.unwrap()))
}

fn is_host_matching(source: &str, target: &str) -> bool {
    !(source.is_empty() || target.is_empty() || source.ne(target))
}

fn get_url_authority(uri_value: &str) -> Option<Authority> {
    let url = uri::Absolute::parse(uri_value);

    url.map_or(None, |url| url.authority().cloned())
}

fn is_authority_matching(source_uri: &str, target_uri: &str) -> bool {
    let source = get_url_authority(source_uri);
    let target = get_url_authority(target_uri);

    source.is_some_and(|source_val| {
        target.is_some_and(|target_val| {
            is_host_matching(source_val.host(), target_val.host())
                && is_port_matching(source_val.port(), target_val.port())
        })
    })
}

#[get("/indie-auth?<me>&<redirect_uri>&<client_id>&<state>&<response_type>")]
async fn authentication_page(
    me: &str,
    redirect_uri: &str,
    client_id: &str,
    state: &str,
    response_type: Option<&str>,
    settings: Settings<'_>,
    config: &State<AppConfig>,
) -> Template {
    let response_type =
        response_type.map_or("id", |value| if value.is_empty() { "id" } else { value });

    Template::render(
        "authentication-page",
        context! {
            is_main: true,
            hide_comments: true,
            settings: &settings,
            choices: &settings.get_choices(),
            meta: &config.card,
            page: context! {
                url: format!("/indie-auth?me={}&redirect_uri={}&client_id={}&state={}&response_type={}", me, redirect_uri, client_id, state, response_type),
                title: "Authentication request",
                desc: "Authenticate another app or website using IndieAuth.",
                me,
                redirect_uri,
                client_id,
                state,
                response_type,
                escaped_uri: format!("/indie-auth?me={}%26redirect_uri={}%26client_id={}%26state={}%26response_type={}", me, redirect_uri, client_id, state, response_type),
            },
            parent: PAGE_TEMPLATE
        },
    )
}

#[post("/indie-auth", data = "<code_verification>")]
async fn auth_verification(
    mut db: Connection<Db>,
    code_verification: Form<IndieAuthCodeVerification>,
    config: &State<AppConfig>,
) -> (Status, Json<IndieAuthResponse>) {
    if !is_authority_matching(
        &code_verification.client_id,
        &code_verification.redirect_uri,
    ) {
        return (
            Status::BadRequest,
            Json(IndieAuthResponse {
                me: None,
                error: Some(String::from("invalid_request")),
            }),
        );
    }

    if let Ok(auth_code) = get_auth_code(&mut db, &code_verification).await {
        delete_auth_code(&mut db, &auth_code.id).await;
        (
            Status::Found,
            Json(IndieAuthResponse {
                me: format!("{}/", config.card.homepage).into(),
                error: None,
            }),
        )
    } else {
        (
            Status::NotFound,
            Json(IndieAuthResponse {
                me: None,
                error: Some(String::from("invalid_grant")),
            }),
        )
    }
}

#[post("/authentication", data = "<indie_auth>")]
async fn authentication(
    mut db: Connection<Db>,
    indie_auth: Form<IndieAuthForm>,
    settings: Settings<'_>,
    config: &State<AppConfig>,
) -> Redirect {
    if !settings.is_logged_in {
        return Redirect::found(format!("{}?error=access_denied", indie_auth.redirect_uri));
    }

    if !is_authority_matching(&indie_auth.client_id, &indie_auth.redirect_uri) {
        return Redirect::to("/invalid-request");
    }

    if !indie_auth.me.starts_with(&config.card.homepage) || indie_auth.state.is_empty() {
        return Redirect::found(format!(
            "{}?error=temporarily_unavailable",
            indie_auth.redirect_uri
        ));
    }

    if let Some(response_type) = &indie_auth.response_type {
        if response_type.ne("id") {
            //TO-DO handle response type = code with a token endpoint.
            print!("Requested for {}", response_type);
            // return Redirect::found(format!(
            //     "{}?error=unsupported_response_type",
            //     indie_auth.redirect_uri
            // ));
        }
    }

    let code = Uuid::new_v4().to_string();
    let now = OffsetDateTime::now_utc();
    let auth_code = IndieAuthCode {
        id: Uuid::new_v4().to_string(),
        client_id: indie_auth.client_id.clone(),
        redirect_uri: indie_auth.redirect_uri.clone(),
        code: code.clone(),
        created_on: now.format(&well_known::Iso8601::DEFAULT).unwrap(),
        expires_on: now
            .checked_add(Duration::minutes(5))
            .unwrap()
            .format(&well_known::Iso8601::DEFAULT)
            .unwrap(),
    };
    if !add_auth_code(&mut db, &auth_code).await {
        return Redirect::found(format!(
            "{}?error=temporarily_unavailable",
            indie_auth.redirect_uri
        ));
    }

    Redirect::found(format!(
        "{}?code={}&state={}",
        indie_auth.redirect_uri, code, indie_auth.state
    ))
}

pub(in crate::routes) fn all_routes() -> Vec<Route> {
    routes![authentication_page, authentication, auth_verification,]
}
