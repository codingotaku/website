/*
Copyright (C) 2021-2030 Coding Otaku (Rahul Sivananda) <contact@codingotaku.com>

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU Affero General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
details.

You should have received a copy of the GNU Affero General Public License along
with this program. If not, see <https://www.gnu.org/licenses/>.
*/

use crate::db::{
    approve_comment, delete_comment, edit_comment, is_valid_login, update_password,
    update_username, Db,
};

use crate::metadata::{Category, FeedType};
use crate::modals::admin::{LoginForm, UpdateForm, User};
use crate::modals::comments::Comment;
use crate::modals::comments::CommentForm;
use crate::modals::page::Pages;
use crate::modals::settings::Settings;
use crate::utils::constants::PAGE_TEMPLATE;
use crate::utils::route_helper::{custom_404, CommonPageRenderer};
use crate::utils::AppConfig;
use rocket::form::Form;
use rocket::http::{Cookie, CookieJar, Status};
use rocket::response::status::Custom;
use rocket::response::Redirect;
use rocket::{get, post, routes, Route, State};
use rocket_db_pools::Connection;
use rocket_dyn_templates::{context, Template};

async fn admin_routes() -> Vec<Route> {
    let admin_pages = Pages::new(Category::Admin).await;
    admin_pages
        .items
        .into_iter()
        .map(|page| {
            Route::new(
                rocket::http::Method::Get,
                &page.url.clone(),
                CommonPageRenderer {
                    page,
                    is_protected: true,
                },
            )
        })
        .collect()
}

#[get("/login-to-site?<redirect_path>")]
async fn login_page<'i>(
    redirect_path: Option<&'i str>,
    settings: Settings<'i>,
    config: &'i State<AppConfig>,
) -> Template {
    Template::render(
        "admin/login-to-site",
        context! {
            is_main: true,
            hide_comments: true,
            settings: &settings,
            choices: &settings.get_choices(),
            meta: &config.card,
            page: context! {
                url:"/login-to-site",
                title: "Login",
                desc:"Login to the website",
                redirect_path,
            },
            feeds: FeedType::get_all_main_urls(&config.card),
            parent: PAGE_TEMPLATE
        },
    )
}

#[get("/admin-page/get-all-comments")]
async fn all_comments(
    settings: Settings<'_>,
    db: Connection<Db>,
    config: &State<AppConfig>,
) -> Custom<Template> {
    if settings.is_logged_in {
        let comments = Comment::get_all_comments(db).await;
        Custom(
            Status::Ok,
            Template::render(
                "admin/admin-page/all-comments",
                context! {
                    is_main: true,
                    hide_comments: true,
                    settings: &settings,
                    choices: &settings.get_choices(),
                    meta: &config.card,
                    page: context! {
                        url:"/admin-page/get-all-comments",
                        title: "All comments",
                        desc:"Moderate the comments added to this website",
                        comments
                    },
                    feeds: FeedType::get_all_main_urls(&config.card),
                    parent: PAGE_TEMPLATE
                },
            ),
        )
    } else {
        custom_404(settings)
    }
}

#[get("/admin-page/edit-comment?<comment_id>")]
async fn edit_selected_comment(
    config: &State<AppConfig>,
    comment_id: &str,
    settings: Settings<'_>,
    db: Connection<Db>,
) -> Custom<Template> {
    if settings.is_logged_in {
        let comment = Comment::get_comment(db, comment_id).await;
        Custom(
            Status::Ok,
            Template::render(
                "admin/admin-page/edit-comment",
                context! {
                    is_main: true,
                    hide_comments: true,
                    settings: &settings,
                    choices: &settings.get_choices(),
                    meta: &config.card,
                    page: context! {
                        url:format!("/admin-page/edit-comment?comment_id={}",comment_id),
                        title: "Edit comment",
                        desc:"Edit or delete a comment.",
                        comment,
                    },
                    feeds: FeedType::get_all_main_urls(&config.card),
                    parent: PAGE_TEMPLATE
                },
            ),
        )
    } else {
        custom_404(settings)
    }
}

#[get("/admin-page/delete-comment?<comment_id>&<sender>")]
async fn delete_selected_comment(
    comment_id: &str,
    sender: Option<String>,
    settings: Settings<'_>,
    mut db: Connection<Db>,
) -> Redirect {
    if settings.is_logged_in && delete_comment(&mut db, comment_id).await {
        if settings.is_logged_in {
            return Redirect::to(
                sender.unwrap_or("/admin-page/get-all-comments".to_string()) + "#comments",
            );
        } else {
            return Redirect::to(sender.unwrap_or("/".to_string()) + "#comments");
        }
    }
    Redirect::to("/404")
}

#[get("/admin-page/approve-comment?<comment_id>")]
async fn approve_selected_comment(
    comment_id: &str,
    settings: Settings<'_>,
    mut db: Connection<Db>,
) -> Redirect {
    if settings.is_logged_in && approve_comment(&mut db, comment_id).await {
        return Redirect::to("/admin-page/get-all-comments");
    }
    Redirect::to("/404")
}

#[post("/admin-page/update-comment", data = "<comment_data>")]
async fn update_comment<'c>(
    comment_data: Form<CommentForm<'c>>,
    mut db: Connection<Db>,
    settings: Settings<'c>,
) -> Redirect {
    let form = Comment::from_form(&comment_data);
    let comment_id = &form.id;

    if settings.is_logged_in {
        if form.comment.is_empty() {
            delete_comment(&mut db, comment_id).await;
            return Redirect::to("/admin-page/get-all-comments");
        } else {
            edit_comment(&mut db, &form).await;
        }
    }

    Redirect::to(format!("/admin-page/get-all-comments#{}", comment_id))
}

fn redirect_to_path(mut optional_path: Option<String>, default: String) -> Redirect {
    let path = optional_path.take_if(|path| path.starts_with("/"));
    Redirect::to(path.unwrap_or(default))
}

#[post("/login-to-site", data = "<form>")]
async fn login(form: Form<LoginForm>, mut db: Connection<Db>, cookies: &CookieJar<'_>) -> Redirect {
    let user = User::from_form(&form);
    if is_valid_login(&mut db, &user.username, &user.password).await {
        cookies.add_private(Cookie::new("user", user.username));
        redirect_to_path(form.redirect_path.clone(), "/".to_string())
    } else if let Some(path) = &form.redirect_path {
        Redirect::to(format!("/login-to-site?redirect_path={}", path))
    } else {
        Redirect::to("/login-to-site")
    }
}

#[get("/logout-from-site")]
fn logout(cookies: &CookieJar<'_>) -> Redirect {
    cookies.remove_private(Cookie::build("user"));
    Redirect::to("/")
}

#[post("/admin-page/update-username", data = "<form>")]
async fn update_current_username(
    form: Form<UpdateForm>,
    mut db: Connection<Db>,
    cookies: &CookieJar<'_>,
) -> Redirect {
    if let Some(user_cookie) = cookies.get_private("user") {
        if form.updated == form.retype
            && is_valid_login(&mut db, user_cookie.value(), &form.password).await
        {
            let user = user_cookie.value();
            let updated = &form.updated;
            if update_username(&mut db, user, updated).await {
                print!("updated {} to {}", user, updated);
                cookies.add_private(Cookie::new("user", form.updated.clone()));
                Redirect::to("/admin-page")
            } else {
                Redirect::to("/admin-page/update-username")
            }
        } else {
            Redirect::to("/admin-page/update-username")
        }
    } else {
        Redirect::to("/admin-page")
    }
}

#[post("/admin-page/update-password", data = "<form>")]
async fn update_current_password(
    form: Form<UpdateForm>,
    mut db: Connection<Db>,
    cookies: &CookieJar<'_>,
) -> Redirect {
    if let Some(user_cookie) = cookies.get_private("user") {
        if form.updated == form.retype
            && is_valid_login(&mut db, user_cookie.value(), &form.password).await
        {
            let user = user_cookie.value();
            let updated = &form.updated;
            if update_password(&mut db, user, updated).await {
                Redirect::to("/admin-page")
            } else {
                Redirect::to("/admin-page/update-password")
            }
        } else {
            Redirect::to("/admin-page/update-password")
        }
    } else {
        Redirect::to("/")
    }
}

pub(in crate::routes) async fn all_routes() -> Vec<Route> {
    let mut routes = routes![
        all_comments,
        approve_selected_comment,
        delete_selected_comment,
        edit_selected_comment,
        login,
        login_page,
        logout,
        update_comment,
        update_current_password,
        update_current_username
    ];
    routes.extend(admin_routes().await);
    routes
}
