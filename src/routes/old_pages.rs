/*
Copyright (C) 2021-2030 Coding Otaku (Rahul Sivananda) <contact@codingotaku.com>

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU Affero General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
details.

You should have received a copy of the GNU Affero General Public License along
with this program. If not, see <https://www.gnu.org/licenses/>.
*/

// This module is here for legacy pages, add the removed or moved page details here

use crate::modals::settings::Settings;
use crate::utils::constants::ERROR_TEMPLATE;
use rocket::http::Status;
use rocket::response::status::Custom;
use rocket::response::Redirect;
use rocket::{get, routes, Route};
use rocket_dyn_templates::{context, Template};

#[get("/tags/<name>?<cat>")]
async fn tags(name: String, cat: String) -> Redirect {
    let mut category: &str = &cat;
    if cat == "1" {
        category = "blogs";
    } else if cat == "2" {
        category = "projects";
    }
    Redirect::moved(format!("/{}/tags/{}", category, name))
}

#[get("/blog/tags/<name>")]
async fn old_blog_tags(name: String) -> Redirect {
    Redirect::moved(format!("/blogs/tags/{}", name))
}

#[get("/project/tags/<name>")]
async fn old_project_tags(name: String) -> Redirect {
    Redirect::moved(format!("/projects/tags/{}", name))
}

#[get("/apps/AnimuDownloaderu")]
async fn old_animu_downloaderu() -> Redirect {
    Redirect::moved("/projects/animu-downloaderu".to_string())
}

#[get("/JEdenManga")]
async fn old_j_eden_manga() -> Redirect {
    Redirect::moved("/projects/j-eden-manga".to_string())
}

#[get("/7-Segment-Digital-Matrix-Rain")]
async fn old_app_7_segment() -> Redirect {
    Redirect::moved("https://apps.codingotaku.com/7-Segment-Digital-Matrix-Rain/".to_string())
}

#[get("/notes")]
async fn notes(settings: Settings<'_>) -> Custom<Template> {
    Custom(
        Status::Gone,
        Template::render(
            ERROR_TEMPLATE,
            context! {
              settings: &settings,
              choices: &settings.get_choices(),
              title: "410: Gone!",
            },
        ),
    )
}

#[get("/about")]
async fn about() -> Redirect {
    Redirect::moved("/".to_string())
}

pub(in crate::routes) fn all_routes() -> Vec<Route> {
    routes![
        about,
        notes,
        old_animu_downloaderu,
        old_app_7_segment,
        old_blog_tags,
        old_j_eden_manga,
        old_project_tags,
        tags
    ]
}
