/*
Copyright (C) 2021-2030 Coding Otaku (Rahul Sivananda) <contact@codingotaku.com>

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU Affero General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
details.

You should have received a copy of the GNU Affero General Public License along
with this program. If not, see <https://www.gnu.org/licenses/>.
*/

use crate::metadata::{Category, FeedType};
use crate::modals::page::{Page, Pages};
use crate::modals::settings::Settings;
use crate::utils::constants::{BOILERPLATE, LIST_TEMPLATE, TAGS_TEMPLATE};
use crate::utils::{AppConfig, Card};
use rocket::serde::json::json;
use rocket::{get, routes, Route, State};
use rocket_dyn_templates::{context, Template};

async fn render_all_tags(category: Category<'_>, settings: Settings<'_>, card: &Card) -> Template {
    let pages = Pages::new(category).await;

    let tags = pages
        .tags
        .iter()
        .map(|tag| {
            (
                tag,
                pages
                    .items
                    .iter()
                    .filter(|&page| page.tags.contains(tag))
                    .count(),
            )
        })
        .collect::<Vec<(&String, usize)>>();

    Template::render(
        TAGS_TEMPLATE,
        context! {
          is_main:true,
          hide_comments: true,
          settings: &settings,
          choices: &settings.get_choices(),
          cat: category.get_type(),
          cat_title: category.get_title_case(),
          page: json!({
            "title": category.get_title_case(),
            "url": format!("{}/tags", category.get_url()),
            "tags": tags,
            "desc": category.generate_list_description()
          }),
          meta: card,
          feeds: FeedType::get_all_category_urls(category, card),
          parent: BOILERPLATE,
        },
    )
}

async fn render_pages_for_tag(
    category: Category<'_>,
    settings: Settings<'_>,
    card: &Card,
) -> Template {
    let pages = Pages::new(category).await;
    let items: Vec<Page> = pages.items;
    let tags: Vec<String> = pages.tags;

    let tag = match category {
        Category::MainsForTag(tag)
        | Category::BlogsForTag(tag)
        | Category::ProjectsForTag(tag)
        | Category::StoriesForTag(tag)
        | Category::ChaptersForTag(_, tag) => tag,
        _ => "",
    };
    Template::render(
        LIST_TEMPLATE,
        context! {
          is_main:true,
          hide_comments: true,
          settings: &settings,
          choices: &settings.get_choices(),
          items,
          page: json!({
            "title": category.get_title_case(),
            "url": category.get_url(),
            "tags": tags,
            "desc": category.generate_list_description(),
            "tag": tag
          }),
          cat: category.get_type(),
          meta: card,
          tags: tags,
          feeds: FeedType::get_all_category_urls(category, card),
          parent: BOILERPLATE,
        },
    )
}

#[get("/main/tags")]
async fn main_tags(settings: Settings<'_>, config: &State<AppConfig>) -> Template {
    render_all_tags(Category::Main, settings, &config.card).await
}

#[get("/all/tags")]
async fn all_tags(settings: Settings<'_>, config: &State<AppConfig>) -> Template {
    render_all_tags(Category::All, settings, &config.card).await
}

#[get("/main/tags/<tag>")]
async fn main_tag_search(
    config: &State<AppConfig>,
    settings: Settings<'_>,
    tag: String,
) -> Template {
    render_pages_for_tag(Category::MainsForTag(&tag), settings, &config.card).await
}

#[get("/all/tags/<tag>")]
async fn tag_search(config: &State<AppConfig>, settings: Settings<'_>, tag: String) -> Template {
    render_pages_for_tag(Category::AllForTag(&tag), settings, &config.card).await
}

#[get("/blogs/tags")]
async fn blog_tags(settings: Settings<'_>, config: &State<AppConfig>) -> Template {
    render_all_tags(Category::Blogs, settings, &config.card).await
}

#[get("/blogs/tags/<tag>")]
async fn blog_tag_search(
    config: &State<AppConfig>,
    settings: Settings<'_>,
    tag: String,
) -> Template {
    render_pages_for_tag(Category::BlogsForTag(&tag), settings, &config.card).await
}

#[get("/projects/tags")]
async fn project_tags(settings: Settings<'_>, config: &State<AppConfig>) -> Template {
    render_all_tags(Category::Projects, settings, &config.card).await
}

#[get("/projects/tags/<tag>")]
async fn project_tag_search(
    config: &State<AppConfig>,
    settings: Settings<'_>,
    tag: String,
) -> Template {
    render_pages_for_tag(Category::ProjectsForTag(&tag), settings, &config.card).await
}

#[get("/stories/tags")]
async fn story_tags(settings: Settings<'_>, config: &State<AppConfig>) -> Template {
    render_all_tags(Category::Stories, settings, &config.card).await
}

#[get("/stories/tags/<tag>")]
async fn story_tag_search(
    config: &State<AppConfig>,
    settings: Settings<'_>,
    tag: String,
) -> Template {
    render_pages_for_tag(Category::StoriesForTag(&tag), settings, &config.card).await
}

#[get("/stories/<story>/tags", rank = 2)]
async fn chapter_tags(
    settings: Settings<'_>,
    story: String,
    config: &State<AppConfig>,
) -> Template {
    render_all_tags(Category::ChaptersForStory(&story), settings, &config.card).await
}

#[get("/stories/<story>/tags/<tag>")]
async fn chapter_tag_search(
    config: &State<AppConfig>,
    settings: Settings<'_>,
    story: String,
    tag: String,
) -> Template {
    render_pages_for_tag(
        Category::ChaptersForTag(&story, &tag),
        settings,
        &config.card,
    )
    .await
}

pub(in crate::routes) fn all_routes() -> Vec<Route> {
    routes![
        all_tags,
        blog_tags,
        blog_tag_search,
        chapter_tags,
        chapter_tag_search,
        main_tags,
        main_tag_search,
        project_tags,
        project_tag_search,
        story_tags,
        story_tag_search,
        tag_search
    ]
}
