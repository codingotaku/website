/*
Copyright (C) 2021-2030 Coding Otaku (Rahul Sivananda) <contact@codingotaku.com>

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU Affero General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
details.

You should have received a copy of the GNU Affero General Public License along
with this program. If not, see <https://www.gnu.org/licenses/>.
 */

extern crate reqwest;

use crate::db::{add_comment, search_table, Db};
use crate::metadata::{Category, FeedType};
use crate::modals::comments::{Comment, Comments};
use crate::modals::page::Pages;
use crate::modals::search::{sanitize_search, SearchPage};
use crate::modals::settings::{Settings, SettingsForm};
use crate::utils::constants;
use crate::utils::db::get_last_login_date;
use crate::utils::route_helper::{custom_404, CommonPageRenderer};
use crate::utils::{AppConfig, Card};
use rocket::futures::future;
use rocket::http::{uri, Cookie, CookieJar, Status};
use rocket::response::status::Custom;
use rocket::response::Redirect;
use rocket::route::Route;
use rocket::serde::json::json;
use rocket::time::format_description::well_known;
use rocket::time::OffsetDateTime;
use rocket::{form::Form, form::FromForm};
use rocket::{get, post, routes, State};
use rocket_db_pools::Connection;
use rocket_dyn_templates::{context, Metadata, Template};
use uuid::Uuid;

async fn render_page_if_exists(
    metadata: Metadata<'_>,
    settings: Settings<'_>,
    comments: Comments,
    name: &str,
    category: Category<'_>,
    card: &Card,
) -> Custom<Template> {
    let url = format!("/{}/{}", category.get_type(), name);

    let pages = Pages::new(category).await;

    let page = pages.find_by_url(&url);

    let Some(page) = page else {
        return custom_404(settings);
    };

    let template = format!("{}/{}", category.get_type(), name);
    if !metadata.contains_template(&template) {
        return custom_404(settings);
    }

    Custom(
        Status::Ok,
        Template::render(
            template,
            context! {
                settings: &settings,
                choices: &settings.get_choices(),
                parent: constants::PAGE_TEMPLATE,
                cat: category.get_type(),
                cat_title: &category.get_title_case(),
                nav: pages.get_page_nav(&url),
                recents: pages.recent_excluding_url(&url),
                page: page,
                meta: card,
                comments: comments.data,
                feeds: FeedType::get_all_category_urls(category, card)
            },
        ),
    )
}

async fn render_list(category: Category<'_>, settings: Settings<'_>, card: &Card) -> Template {
    let pages = Pages::new(category).await;

    Template::render(
        constants::LIST_TEMPLATE,
        context! {
          is_main: true,
          settings: &settings,
          choices: &settings.get_choices(),
          page: context!{
            url: category.get_url(),
            tags: pages.tags,
            desc: category.generate_list_description(),
            title: category.get_title_case(),
          },
          cat: category.get_type(),
          cat_title: category.get_title_case(),
          meta: card,
          items: pages.items,
          feeds: FeedType::get_all_category_urls(category, card),
          parent: constants::BOILERPLATE
        },
    )
}

async fn render_story_info(
    category: Category<'_>,
    story: &str,
    settings: Settings<'_>,
    card: &Card,
) -> Custom<Template> {
    let path = format!("/{}/{}", category.get_type(), story);
    let chapter_category = Category::ChaptersForStory(story);

    let (chapters, stories) =
        future::join(Pages::new(chapter_category), Pages::new(Category::Stories)).await;

    if let Some(story) = stories.find_by_url(&path) {
        Custom(
            Status::Ok,
            Template::render(
                constants::STORIES_TEMPLATE,
                context! {
                  is_main: true,
                  settings: &settings,
                  choices: &settings.get_choices(),
                  title: &story.title,
                  page: &story,
                  cat: category.get_type(),
                  meta: card,
                  sub_cat: chapter_category.get_type(),
                  cat_title: category.get_title_case(),
                  items: &chapters.items,
                  oldest: chapters.items.last(),
                  tags: &story.tags,
                  feeds: FeedType::get_all_category_urls(chapter_category, card)
                },
            ),
        )
    } else {
        custom_404(settings)
    }
}

#[get("/blogs")]
async fn blogs(settings: Settings<'_>, config: &State<AppConfig>) -> Template {
    render_list(Category::Blogs, settings, &config.card).await
}

#[get("/all")]
async fn all_pages(settings: Settings<'_>, config: &State<AppConfig>) -> Template {
    render_list(Category::All, settings, &config.card).await
}

#[get("/main")]
async fn main_pages(settings: Settings<'_>, config: &State<AppConfig>) -> Template {
    render_list(Category::Main, settings, &config.card).await
}

#[get("/blogs/<name>")]
async fn blog(
    settings: Settings<'_>,
    comments: Comments,
    metadata: Metadata<'_>,
    name: &str,
    config: &State<AppConfig>,
) -> Custom<Template> {
    render_page_if_exists(
        metadata,
        settings,
        comments,
        name,
        Category::Blogs,
        &config.card,
    )
    .await
}

#[get("/stories")]
async fn stories(config: &State<AppConfig>, settings: Settings<'_>) -> Template {
    render_list(Category::Stories, settings, &config.card).await
}

#[get("/stories/<story>")]
async fn chapters(
    config: &State<AppConfig>,
    settings: Settings<'_>,
    story: &str,
) -> Custom<Template> {
    render_story_info(Category::Stories, story, settings, &config.card).await
}

#[get("/stories/<story>/<chapter>", rank = 3)]
async fn chapter(
    config: &State<AppConfig>,
    settings: Settings<'_>,
    comments: Comments,
    metadata: Metadata<'_>,
    story: &str,
    chapter: &str,
) -> Custom<Template> {
    render_page_if_exists(
        metadata,
        settings,
        comments,
        chapter,
        Category::ChaptersForStory(story),
        &config.card,
    )
    .await
}

#[get("/projects")]
async fn projects(config: &State<AppConfig>, settings: Settings<'_>) -> Template {
    render_list(Category::Projects, settings, &config.card).await
}

#[get("/projects/<name>")]
async fn project(
    config: &State<AppConfig>,
    settings: Settings<'_>,
    comments: Comments,
    metadata: Metadata<'_>,
    name: &str,
) -> Custom<Template> {
    render_page_if_exists(
        metadata,
        settings,
        comments,
        name,
        Category::Projects,
        &config.card,
    )
    .await
}

#[get("/search?<text>")]
async fn search(
    text: Option<&str>,
    mut db: Connection<Db>,
    settings: Settings<'_>,
    config: &State<AppConfig>,
) -> Custom<Template> {
    let query = text.map_or("".to_string(), sanitize_search);

    let result = if !query.is_empty() {
        let pages = search_table(&mut db, &query)
            .await
            .unwrap_or(Vec::<SearchPage>::new());

        let title = format!(
            r#"Found {} result(s) for the query "{}""#,
            pages.len(),
            &query
        );

        json!({
            "title":title,
            "pages": pages,
            "url": format!("search?text={}", &query),
        })
    } else {
        json!({
            "title": String::from("You can search my website here!"),
            "pages": Vec::<SearchPage>::new(),
            "url": String::from("search"),
        })
    };

    Custom(
        Status::Ok,
        Template::render(
            constants::SEARCH_TEMPLATE,
            context! {
                is_main: true,
                hide_comments: true,
                settings: &settings,
                choices: &settings.get_choices(),
                page: context! {
                    title: &result["title"],
                    desc: &result["title"],
                    url: &result["url"],
                },
                meta: &config.card,
                input: query,
                items: &result["pages"]
            },
        ),
    )
}

#[get("/am-I-alive")]
async fn am_i_alive(
    config: &State<AppConfig>,
    mut db: Connection<Db>,
    settings: Settings<'_>,
    comments: Comments,
) -> Custom<Template> {
    let last_login = get_last_login_date(&mut db, "otaku").await;
    Custom(
        Status::Ok,
        Template::render(
            "am-I-alive",
            context! {
                is_main: true,
                settings: &settings,
                comments: comments.data,
                choices: &settings.get_choices(),
                page: context! {
                    title: "Am I Alive?",
                    desc: "Watch this page to know whether I am just not updating this website or has the lost capacity to do so.",
                    url: "am-I-alive",
                    tags: ["contact"],
                    date: "2024-05-18T10:19:07Z",
                    last_updated: last_login
                },
                meta: &config.card,
                parent: constants::PAGE_TEMPLATE,
                feeds: FeedType::get_all_main_urls(&config.card)
            },
        ),
    )
}

async fn common_routes() -> Vec<Route> {
    let main_pages = Pages::new(Category::Main).await;

    main_pages
        .items
        .into_iter()
        .map(|page| {
            Route::new(
                rocket::http::Method::Get,
                &page.url.clone(),
                CommonPageRenderer {
                    page,
                    is_protected: false,
                },
            )
        })
        .collect()
}

#[derive(FromForm)]
pub(crate) struct WebmentionForm {
    pub source: String,
    pub target: String,
}

#[post("/webmention", data = "<webmention_data>")]
async fn webmention(
    webmention_data: Form<WebmentionForm>,
    config: &State<AppConfig>,
    mut db: Connection<Db>,
) -> (Status, String) {
    let absolute_source_url = uri::Absolute::parse(&webmention_data.source);
    let absolute_target_url = uri::Absolute::parse(&webmention_data.target);

    if absolute_source_url.is_err() {
        return (
            Status::BadRequest,
            format!(
                r#"The source URL "{}" is not an absolute URL"#,
                webmention_data.source
            ),
        );
    }

    if absolute_target_url.is_err() {
        return (
            Status::BadRequest,
            format!(
                r#"The target URL "{}" is not an absolute URL"#,
                webmention_data.target
            ),
        );
    }

    let absolute_source_url = absolute_source_url.unwrap();
    let absolute_target_url = absolute_target_url.unwrap();
    let source_authority = absolute_source_url.authority();
    let target_authority = absolute_target_url.authority();

    if source_authority.is_none() {
        return (
            Status::BadRequest,
            format!(
                r#"The source URL "{}" is invalid (or I missed something)"#,
                webmention_data.source
            ),
        );
    }

    if target_authority.is_none() {
        return (
            Status::BadRequest,
            format!(
                r#"The target URL "{}" is invalid (or I missed something)"#,
                webmention_data.target
            ),
        );
    }

    let source_authority = source_authority.unwrap();

    let homepage = &config.card.homepage;
    if !webmention_data.target.starts_with(homepage) {
        (
            Status::BadRequest,
            format!(
                r#"Invalid target URL "{}", this should be pointing to "{}" instead."#,
                webmention_data.target, homepage
            ),
        )
    } else if webmention_data.target.eq(&webmention_data.source) {
        (
            Status::BadRequest,
            "The source and target URLS are the same!".to_string(),
        )
    } else {
        let target_path = absolute_target_url.path().to_string();

        let comment = Comment {
            id: Uuid::new_v4().to_string(),
            is_moderated: 0,
            name: source_authority.to_string(),
            website: format!("{}://{}", absolute_target_url.scheme(), source_authority),
            source: absolute_source_url.to_string(),
            comment: "dummy comment, replace it with the one from mention".to_string(),
            mention_date: OffsetDateTime::now_utc()
                .format(&well_known::Iso8601::DEFAULT)
                .unwrap(),
            mention_type: Option::None,
            post: target_path,
        };

        if add_comment(&mut db, &comment).await {
            #[cfg(feature = "notification")]
            if let Some(mention) = &config.mention {
                let notify_url = &mention.notify_url;
                if !notify_url.is_empty() {
                    let message = comment.to_message();
                    let client = reqwest::Client::new();
                    let _ = client.post(notify_url).body(message).send().await;
                }
            }
        }

        (
            Status::Accepted,
            "Thank you, The request has been accepted for further processing and moderation."
                .to_string(),
        )
    }
}

#[post("/settings", data = "<message>")]
async fn settings(cookies: &CookieJar<'_>, message: Form<SettingsForm<'_>>) -> Redirect {
    let form = Settings::from_form(message);

    if form.clear {
        cookies
            .iter()
            .for_each(|cookie| cookies.remove_private(cookie.to_owned()));
    } else {
        cookies.add_private(Cookie::new("theme", form.theme.value));
        cookies.add_private(Cookie::new("width", form.width.value));
        cookies.add_private(Cookie::new("opener", form.opener.value));
        cookies.add_private(Cookie::new("size", form.size.value));
        cookies.add_private(Cookie::new("settings_status", "open"));
    }

    Redirect::to(form.sender.unwrap_or("/".to_string()))
}

pub(in crate::routes) async fn all_routes() -> Vec<Route> {
    let mut routes = routes![
        am_i_alive, all_pages, blog, blogs, chapter, chapters, main_pages, project, projects,
        search, settings, stories, webmention
    ];
    routes.extend(common_routes().await);
    routes
}
