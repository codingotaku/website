/*
Copyright (C) 2021-2030 Coding Otaku (Rahul Sivananda) <contact@codingotaku.com>

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU Affero General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
details.

You should have received a copy of the GNU Affero General Public License along
with this program. If not, see <https://www.gnu.org/licenses/>.
*/

use rocket::serde::{Deserialize, Serialize};

pub(crate) mod constants;
pub(crate) mod db;
pub(crate) mod helper;
pub(crate) mod route_helper;

#[derive(Deserialize, Default, Serialize)]
#[serde(crate = "rocket::serde")]
#[cfg(feature = "notification")]
pub(crate) struct CommentConfig {
    pub(crate) notify_url: String,
}

#[derive(Deserialize, Default, Serialize)]
#[serde(crate = "rocket::serde")]
pub(crate) struct Card {
    pub(crate) fullname: String,
    pub(crate) display_name: String,
    pub(crate) homepage: String,
    pub(crate) email: String,
}

#[derive(Deserialize, Default, Serialize)]
#[serde(crate = "rocket::serde")]
pub(crate) struct AppConfig {
    pub(crate) card: Card,
    #[cfg(feature = "notification")]
    pub(crate) mention: Option<CommentConfig>,
}
