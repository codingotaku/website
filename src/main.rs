/*
Copyright (C) 2021-2030 Coding Otaku (Rahul Sivananda) <contact@codingotaku.com>

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU Affero General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
details.

You should have received a copy of the GNU Affero General Public License along
with this program. If not, see <https://www.gnu.org/licenses/>.
*/

mod metadata;
mod modals;
mod routes;
mod utils;

use crate::utils::constants::ERROR_TEMPLATE;
use modals::settings::Settings;
use rocket::fairing::AdHoc;
use rocket::figment::providers::Serialized;
use rocket::figment::{
    providers::{Env, Format, Toml},
    Figment,
};
use rocket::fs::{relative, FileServer, Options};
use rocket::{catch, catchers, launch, Request};
use rocket_dyn_templates::{context, Template};
use routes::all_routes;
use utils::{db, helper, AppConfig};

#[catch(404)]
async fn not_found(req: &Request<'_>) -> Template {
    let settings = req.guard::<Settings>().await.unwrap();

    Template::render(
        ERROR_TEMPLATE,
        context! {
            is_main: true,
            settings: &settings,
            choices: &settings.get_choices(),
            page: context!{
                title: "404: Not Found!"
            },
        },
    )
}

#[catch(default)]
async fn default_catcher(req: &Request<'_>) -> Template {
    let settings = req.guard::<Settings>().await.unwrap();
    Template::render(
        ERROR_TEMPLATE,
        context! {
            is_main: true,
            settings: &settings,
            choices: &settings.get_choices(),
            page: context!{
                title: "Umm What did ya try?"
            },
        },
    )
}

#[launch]
async fn rocket() -> _ {
    let figment = Figment::from(rocket::Config::default())
        .merge(Toml::file("App.toml").nested())
        .merge(Env::prefixed("APP_").global())
        .merge(Serialized::default("website", AppConfig::default()));
    let options = Options::Index | Options::DotFiles;
    rocket::custom(figment)
        .register("/", catchers![not_found, default_catcher])
        .mount("/", all_routes().await)
        .mount("/", FileServer::new(relative!("static"), options))
        .attach(Template::custom(|engines| {
            helper::handlebar_helpers(&mut engines.handlebars);
        }))
        .attach(AdHoc::config::<AppConfig>())
        .attach(db::db_fairing())
        .attach(db::build_search_index())
}
