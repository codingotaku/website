/*
Copyright (C) 2021-2030 Coding Otaku (Rahul Sivananda) <contact@codingotaku.com>

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU Affero General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
details.

You should have received a copy of the GNU Affero General Public License along
with this program. If not, see <https://www.gnu.org/licenses/>.
*/

use regex::Regex;
use rocket::{
    http::{uri::Origin, RawStr},
    serde::{
        json::{json, Value},
        Serialize,
    },
    time::{
        format_description::{self, well_known},
        OffsetDateTime,
    },
};

use rocket_dyn_templates::handlebars::{
    html_escape, Context, Handlebars, Helper, HelperResult, Output, RenderContext, Renderable,
};

use crate::modals::page::Toc;

pub(crate) fn strip_date(
    helper: &Helper<'_>,
    _: &Handlebars,
    _: &Context,
    _: &mut RenderContext<'_, '_>,
    out: &mut dyn Output,
) -> HelperResult {
    let date_time = helper.param(0).unwrap().render();

    if let Ok(time) = OffsetDateTime::parse(&date_time, &well_known::Iso8601::DEFAULT) {
        let formatted_time = time
            .format(
                &format_description::parse(
                    "[year] [month repr:short] [day] at [hour repr:12]:[minute] [period]",
                )
                .unwrap(),
            )
            .unwrap();
        out.write(&formatted_time)?;
    }

    Ok(())
}

fn print_page(
    helper: &Helper<'_>,
    handlebars: &Handlebars,
    _: &Context,
    _: &mut RenderContext<'_, '_>,
    out: &mut dyn Output,
) -> HelperResult {
    let url = helper.param(1).unwrap().render();
    let template = if url.eq("/") { "/home" } else { &url }.replacen('/', "", 1);

    let res = handlebars
        .render(
            &template,
            &json!({
                "parent": "feeds/blank",
                "url": url,
                "banner": helper.param(2).unwrap().value()
            }),
        )
        .unwrap();
    out.write(&res)?;
    Ok(())
}

#[derive(Serialize)]
#[serde(crate = "rocket::serde")]
struct Breadcrump {
    path: String,
    value: String,
}

fn breadcrumbs(
    _: &Helper<'_>,
    handlebars: &Handlebars,
    context: &Context,
    _: &mut RenderContext<'_, '_>,
    out: &mut dyn Output,
) -> HelperResult {
    let Some(page) = context.data().get("page") else {
        return Ok(());
    };

    let Some(url) = page.get("url") else {
        return Ok(());
    };

    let url = url.as_str().unwrap();
    let relative_path = url.replace(' ', "+");
    let Ok(relative_path) = Origin::parse(&relative_path) else {
        return Ok(());
    };
    let path_segments = relative_path.path().as_str().split('/');
    let mut last_path = Vec::<&str>::new();
    let paths = path_segments
        .into_iter()
        .filter_map(|path| {
            if path.is_empty() {
                return None;
            }
            last_path.push(path);
            Some(Breadcrump {
                path: last_path.join("/"),
                value: path.to_string(),
            })
        })
        .collect::<Vec<Breadcrump>>();
    let res = handlebars
        .render(
            "partials/breadcrumb",
            &json!({
                "url": url.replacen('/', "", 1),
                "paths": paths
            }),
        )
        .unwrap();
    out.write(&res)?;
    Ok(())
}

fn print_toc(
    _: &Helper<'_>,
    handlebars: &Handlebars,
    context: &Context,
    render_context: &mut RenderContext<'_, '_>,
    out: &mut dyn Output,
) -> HelperResult {
    let template = render_context.get_root_template_name().unwrap();
    let regex =
        Regex::new(r#"<h([1-6]) id="([^"]+)">\s*(?:<a[^>]+>)?(.+?)(?:</a>)?\s*</h[1-6]>"#).unwrap();

    let mut data = context.data().clone();
    *data.get_mut("parent").unwrap() = Value::String(String::from("partials/blank"));

    let content = handlebars.render(template, &data).unwrap();

    let matches = regex.captures_iter(&content);

    let table_of_contents = Toc::from(matches);

    if table_of_contents.len() > 2 {
        let res = handlebars
            .render(
                "partials/toc",
                &json!({
                    "contents": Toc::display(&table_of_contents)
                }),
            )
            .unwrap();
        out.write(&res)?;
    }

    Ok(())
}

fn url_escape(
    helper: &Helper<'_>,
    _: &Handlebars,
    _: &Context,
    _: &mut RenderContext<'_, '_>,
    out: &mut dyn Output,
) -> HelperResult {
    let text = helper.param(0).unwrap().render();
    let raw_str = RawStr::new(&text);
    out.write(&raw_str.percent_encode().to_string())?;
    Ok(())
}

fn inline_code_format<'s>(is_feed: bool) -> &'s str {
    if is_feed {
        "<code>"
    } else {
        r#"<code class="code" spellcheck="false" autocapitalize="none" translate="no" contenteditable="true" >"#
    }
}

fn inline_code<'registry, 'render_context>(
    helper: &Helper<'render_context>,
    registry: &'registry Handlebars<'registry>,
    context: &'render_context Context,
    render_context: &mut RenderContext<'registry, 'render_context>,
    out: &mut dyn Output,
) -> HelperResult {
    let is_feed = context.data().get("banner").is_some(); // ugly, but we do not need spellcheck and stuff for feeds
    if let Some(param) = helper.template() {
        let code = param
            .renders(registry, context, render_context)
            .unwrap_or(String::new());
        let mut output = inline_code_format(is_feed).to_string();
        output.push_str(&html_escape(&code));
        output.push_str("</code>");
        out.write(&output)?
    }
    Ok(())
}

fn block_code<'registry, 'render_context>(
    helper: &Helper<'render_context>,
    registry: &'registry Handlebars<'registry>,
    context: &'render_context Context,
    render_context: &mut RenderContext<'registry, 'render_context>,
    out: &mut dyn Output,
) -> HelperResult {
    if let Some(param) = helper.template() {
        let is_feed = context.data().get("banner").is_some(); // ugly, but we do not need spellcheck and stuff for feeds

        let code = param
            .renders(registry, context, render_context)
            .unwrap_or(String::new());

        let mut output = if is_feed {
            "<pre><code>"
        } else {
            r#"<pre spellcheck="false" autocapitalize="none" translate="no" contenteditable="true"><code>"#
        }.to_string();

        output.push_str(&html_escape(&code));
        output.push_str("</code></pre>\n");
        out.write(&output)?;
    }

    Ok(())
}

pub(crate) fn handlebar_helpers(handlebars: &mut Handlebars) {
    handlebars.register_helper("code", Box::new(block_code));
    handlebars.register_helper("inline-code", Box::new(inline_code));
    handlebars.register_helper("print-page", Box::new(print_page));
    handlebars.register_helper("print-toc", Box::new(print_toc));
    handlebars.register_helper("strip-date", Box::new(strip_date));
    handlebars.register_helper("url-escape", Box::new(url_escape));
    handlebars.register_helper("breadcrumbs", Box::new(breadcrumbs));
}
