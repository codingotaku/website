/*
Copyright (C) 2021-2030 Coding Otaku (Rahul Sivananda) <contact@codingotaku.com>

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU Affero General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
details.

You should have received a copy of the GNU Affero General Public License along
with this program. If not, see <https://www.gnu.org/licenses/>.
*/

use crate::modals::comments::Comment;
use crate::modals::indie_auth::{IndieAuthCode, IndieAuthCodeVerification};
use crate::modals::search::{get_match_context, get_search_templates, split_tags, SearchPage};
use rocket::fairing::AdHoc;
use rocket::futures::TryFutureExt;
use rocket_db_pools::{sqlx, sqlx::Row, Connection, Database};

use sha3::{Digest, Sha3_512};
use uuid::Uuid;

#[derive(Database)]
#[database("sqlite")]
pub(crate) struct Db(sqlx::SqlitePool);

type Result<T, E = sqlx::Error> = std::result::Result<T, E>;

async fn get_salt(db: &mut Connection<Db>, user: &str) -> Option<String> {
    sqlx::query("SELECT salt FROM accounts WHERE username = ?")
        .bind(user)
        .fetch_one(&mut ***db)
        .await
        .and_then(|row| row.try_get(0))
        .ok()
}

pub(crate) async fn get_last_login_date(db: &mut Connection<Db>, username: &str) -> Option<String> {
    sqlx::query("SELECT last_login_date FROM accounts WHERE username = ?")
        .bind(username)
        .fetch_one(&mut ***db)
        .await
        .and_then(|row| row.try_get(0))
        .ok()
}

pub(crate) async fn is_valid_login(
    db: &mut Connection<Db>,
    username: &str,
    password: &str,
) -> bool {
    let salt = get_salt(db, username)
        .await
        .unwrap_or("invalid-salt".to_string());

    let mut sha = Sha3_512::new();
    sha.update(password.as_bytes());
    sha.update(salt);
    let salted_pass = hex::encode(sha.finalize());

    let result = sqlx::query("SELECT rowid FROM accounts WHERE username == ? AND password = ?")
        .bind(username)
        .bind(salted_pass)
        .fetch_one(&mut ***db)
        .await
        .is_ok();

    if result {
        let _ = sqlx::query("UPDATE accounts SET last_login_date=strftime('%Y-%m-%dT%H:%M:%SZ', 'now') WHERE username=?")
            .bind(username).execute(&mut ***db).await;
    }
    result
}

pub(crate) async fn update_username(db: &mut Connection<Db>, user: &str, updated: &str) -> bool {
    sqlx::query("UPDATE accounts set username=? WHERE username=?")
        .bind(updated)
        .bind(user)
        .execute(&mut ***db)
        .await
        .is_ok()
}

pub(crate) async fn fetch_comments(
    db: &mut Connection<Db>,
    post: &str,
) -> Result<Vec<Comment>, sqlx::Error> {
    let result: Vec<Comment> = sqlx::query(
        "SELECT comment, source, mention_date, id, is_moderated, name, post, mention_type, website
               FROM mentions
               WHERE post=?
               AND is_moderated=1
               ORDER BY rowid",
    )
    .bind(post)
    .fetch_all(&mut ***db)
    .map_ok(|rows| {
        rows.into_iter().map(|row| Comment {
            comment: row.get("comment"),
            source: row.get("source"),
            mention_date: row.get("mention_date"),
            id: row.get("id"),
            is_moderated: row.get("is_moderated"),
            name: row.get("name"),
            post: row.get("post"),
            mention_type: row.get("mention_type"),
            website: row.get("website"),
        })
    })
    .await?
    .collect();

    Ok(result)
}

pub(crate) async fn fetch_all_comments(
    db: &mut Connection<Db>,
) -> Result<Vec<Comment>, sqlx::Error> {
    let result: Vec<Comment> = sqlx::query(
        "SELECT comment, source, mention_date, id, is_moderated, name, post, mention_type, website
               FROM mentions
               ORDER BY rowid DESC",
    )
    .fetch_all(&mut ***db)
    .map_ok(|rows| {
        rows.into_iter().map(|row| Comment {
            comment: row.get("comment"),
            source: row.get("source"),
            mention_date: row.get("mention_date"),
            id: row.get("id"),
            is_moderated: row.get("is_moderated"),
            name: row.get("name"),
            post: row.get("post"),
            mention_type: row.get("mention_type"),
            website: row.get("website"),
        })
    })
    .await?
    .collect();
    Ok(result)
}

pub(crate) async fn fetch_comment(
    db: &mut Connection<Db>,
    id: &str,
) -> Result<Comment, sqlx::Error> {
    sqlx::query(
        "SELECT comment, source, mention_date, id, is_moderated, name, post, mention_type, website
               FROM mentions
               WHERE id=?
               ORDER BY rowid",
    )
    .bind(id)
    .fetch_one(&mut ***db)
    .map_ok(|row| Comment {
        comment: row.get("comment"),
        source: row.get("source"),
        mention_date: row.get("mention_date"),
        id: row.get("id"),
        is_moderated: row.get("is_moderated"),
        name: row.get("name"),
        post: row.get("post"),
        mention_type: row.get("mention_type"),
        website: row.get("website"),
    })
    .await
}

pub(crate) async fn delete_comment(db: &mut Connection<Db>, id: &str) -> bool {
    sqlx::query("DELETE FROM mentions WHERE id=?")
        .bind(id)
        .execute(&mut ***db)
        .await
        .is_ok()
}

pub(crate) async fn edit_comment(db: &mut Connection<Db>, comment: &Comment) -> bool {
    sqlx::query("UPDATE mentions set name=?, source=?, website=?, mention_date=?, mention_type=?, comment=? WHERE id=?")
        .bind(&comment.name)
        .bind(&comment.source)
        .bind(&comment.website)
        .bind(&comment.mention_date)
        .bind(&comment.mention_type)
        .bind(&comment.comment)
        .bind(&comment.id)
        .execute(&mut ***db)
        .await
        .is_ok()
}

pub(crate) async fn approve_comment(db: &mut Connection<Db>, comment_id: &str) -> bool {
    sqlx::query("UPDATE mentions set is_moderated=1 WHERE id=?")
        .bind(comment_id)
        .execute(&mut ***db)
        .await
        .is_ok()
}

pub(crate) async fn add_comment(db: &mut Connection<Db>, comment: &Comment) -> bool {
    sqlx::query("INSERT into mentions (id, post, name, source, website, comment, mention_type, is_moderated, mention_date) values(?, ?, ?, ?, ?, ?, ?, ?, ?)")
        .bind(&comment.id)
        .bind(&comment.post)
        .bind(&comment.name)
        .bind(&comment.source)
        .bind(&comment.website)
        .bind(&comment.comment)
        .bind(&comment.mention_type)
        .bind(comment.is_moderated)
        .bind(&comment.mention_date)
        .execute(&mut ***db).await.is_ok()
}

pub(crate) async fn update_password(db: &mut Connection<Db>, user: &str, updated: &str) -> bool {
    let salt = Uuid::new_v4().to_string();
    let mut sha = Sha3_512::new();
    sha.update(updated.as_bytes());
    sha.update(salt.as_bytes());
    let hashed_password = hex::encode(sha.finalize());

    sqlx::query("UPDATE accounts set password=?, salt=? WHERE username=?")
        .bind(hashed_password)
        .bind(salt)
        .bind(user)
        .execute(&mut ***db)
        .await
        .is_ok()
}

pub(crate) async fn does_user_exist(db: &mut Connection<Db>, username: &str) -> bool {
    sqlx::query("SELECT rowid FROM accounts WHERE username == ?")
        .bind(username)
        .fetch_one(&mut ***db)
        .await
        .is_ok()
}

pub(crate) async fn search_table(
    db: &mut Connection<Db>,
    text: &str,
) -> Result<Vec<SearchPage>, sqlx::Error> {
    let result = sqlx::query("SELECT url, highlight(searchpage, 1, '<mark>','</mark>'), highlight(searchpage, 2, '<mark>','</mark>'), highlight(searchpage, 3, '<mark>','</mark>'), highlight(searchpage, 4, '<mark>','</mark>') FROM searchpage(?) ORDER BY rank")
        .bind(text)
        .fetch_all(&mut ***db)
        .map_ok_or_else(|_|{
            Vec::<SearchPage>::new()
        },|rows| {
            rows.iter().map(|row| {
                SearchPage {
                    url: row.get(0),
                    title: row.get(1),
                    summary: row.get(2),
                    tags: split_tags(row.get(3)),
                    content: get_match_context(row.get(4)),
                }
            }).collect()
        })
        .await;

    Ok(result)
}

async fn create_search_db(db: &Db) {
    /*
     * This should use migration instead, but it force me to add other dependencies
     * See: https://github.com/launchbadge/sqlx/issues/3211 and https://github.com/rust-lang/cargo/issues/10801
     */

    let result = sqlx::raw_sql("
               CREATE VIRTUAL TABLE IF NOT EXISTS searchpage USING fts5(url, title, summary, tags, content, tokenize = 'porter unicode61');
               CREATE TABLE IF NOT EXISTS accounts (username TEXT NOT NULL, password TEXT NOT NULL, salt TEXT NOT NULL, last_login_date DATETIME);
               CREATE TABLE IF NOT EXISTS mentions (id TEXT NOT NULL, post TEXT NOT NULL, name TEXT DEFAULT 'anonymous', source TEXT, website TEXT, comment TEXT NOT NULL, is_moderated INTEGER NOT NULL, mention_date TEXT NOT NULL, mention_type TEXT);
               CREATE TABLE IF NOT EXISTS indie_auth_code (id TEXT NOT NULL, client_id TEXT NOT NULL, redirect_uri TEXT NOT NULL, code TEXT NOT NULL, created_on DATETIME NOT NULL, expires_on DATETIME NOT NULL, UNIQUE(code));"
    ).execute(& **db).await;

    if let Err(error) = result {
        panic!("{}", error);
    }
}

async fn get_urls(db: &Db) -> Result<Vec<String>, sqlx::Error> {
    let result = sqlx::query("SELECT url FROM searchpage")
        .fetch_all(&**db)
        .map_ok_or_else(
            |_| Vec::<String>::new(),
            |rows| rows.iter().map(|row| row.get(0)).collect(),
        )
        .await;
    Ok(result)
}

pub(crate) async fn add_auth_code(db: &mut Connection<Db>, auth_code: &IndieAuthCode) -> bool {
    sqlx::query("INSERT into indie_auth_code (id, client_id, redirect_uri, code, created_on, expires_on) values(?, ?, ?, ?, ?, ?)")
        .bind(&auth_code.id)
        .bind(&auth_code.client_id)
        .bind(&auth_code.redirect_uri)
        .bind(&auth_code.code)
        .bind(&auth_code.created_on)
        .bind(&auth_code.expires_on)
        .execute(&mut ***db).await.is_ok()
}

pub(crate) async fn get_auth_code(
    db: &mut Connection<Db>,
    verification: &IndieAuthCodeVerification,
) -> Result<IndieAuthCode, sqlx::Error> {
    sqlx::query(
        "SELECT id, client_id, redirect_uri, code, created_on, expires_on
               FROM indie_auth_code
               WHERE code=?
               AND client_id=?
               AND redirect_uri=?
               AND datetime(expires_on) > datetime('now');",
    )
    .bind(&verification.code)
    .bind(&verification.client_id)
    .bind(&verification.redirect_uri)
    .fetch_one(&mut ***db)
    .map_ok(|row| IndieAuthCode {
        id: row.get("id"),
        client_id: row.get("client_id"),
        redirect_uri: row.get("redirect_uri"),
        code: row.get("code"),
        created_on: row.get("created_on"),
        expires_on: row.get("expires_on"),
    })
    .await
}

pub(crate) async fn delete_auth_code(db: &mut Connection<Db>, id: &str) -> bool {
    sqlx::query("DELETE from indie_auth_code where id=?")
        .bind(id)
        .execute(&mut ***db)
        .await
        .is_ok()
}

async fn initialize_db(connection: &Db, pages: Vec<SearchPage>) -> Result<(), sqlx::Error> {
    let urls = get_urls(connection).await.unwrap();

    for page in pages {
        if !urls.contains(&page.url) {
            sqlx::query("INSERT INTO searchpage (url, title, summary, tags, content) VALUES (?, ?, ?, ?, ?)")
        } else {
            sqlx::query("UPDATE searchpage set url=?, title=?, summary=?, tags=?, content=? WHERE url==?")
        }.bind(&page.url)
            .bind(&page.title)
            .bind(&page.summary)
            .bind(page.tags.join(","))
            .bind(&page.content)
            .bind(&page.url)
            .execute( &**connection).await?;
    }
    Ok(())
}

pub(crate) fn db_fairing() -> AdHoc {
    AdHoc::on_ignite("Db Initialization", |rocket| async {
        rocket.attach(Db::init())
    })
}

pub(crate) fn build_search_index() -> AdHoc {
    AdHoc::on_liftoff("Build index", |rocket| {
        Box::pin(async move {
            let connection = Db::fetch(rocket).unwrap();
            create_search_db(connection).await;
            let pages = get_search_templates(rocket).await;
            let _ = initialize_db(connection, pages).await;
        })
    })
}
