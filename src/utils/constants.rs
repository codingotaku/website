pub(crate) const ERROR_TEMPLATE: &str = "error-template";
pub(crate) const BOILERPLATE: &str = "boilerplate";
pub(crate) const LIST_TEMPLATE: &str = "list-template";
pub(crate) const PAGE_TEMPLATE: &str = "page-template";
pub(crate) const SEARCH_TEMPLATE: &str = "search-template";
pub(crate) const STORIES_TEMPLATE: &str = "stories-template";
pub(crate) const TAGS_TEMPLATE: &str = "tags-template";
