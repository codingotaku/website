/*
Copyright (C) 2021-2030 Coding Otaku (Rahul Sivananda) <contact@codingotaku.com>

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU Affero General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
details.

You should have received a copy of the GNU Affero General Public License along
with this program. If not, see <https://www.gnu.org/licenses/>.
*/

use crate::metadata::{Category, FeedType};
use crate::modals::comments::Comments;
use crate::modals::page::Page;
use crate::modals::settings::Settings;
use crate::utils::constants::{ERROR_TEMPLATE, PAGE_TEMPLATE};
use rocket::http::Status;
use rocket::response::status::Custom;
use rocket::route::{Handler, Outcome};
use rocket::{Data, Request, State};
use rocket_dyn_templates::{context, Template};

use super::AppConfig;

pub(crate) fn custom_404(settings: Settings<'_>) -> Custom<Template> {
    Custom(
        Status::NotFound,
        Template::render(
            ERROR_TEMPLATE,
            context! {
              settings: &settings,
              choices: &settings.get_choices(),
              page: context!{
                title: "404: Not Found!"
              },
            },
        ),
    )
}

pub(crate) fn custom_401(settings: &Settings<'_>) -> Template {
    Template::render(
        ERROR_TEMPLATE,
        context! {
          settings: settings,
          choices: settings.get_choices(),
          page: context!{
            title: "401: unauthorized"
          },
        },
    )
}

#[derive(Clone)]
pub(crate) struct CommonPageRenderer {
    pub(crate) page: Page,
    pub(crate) is_protected: bool,
}

impl CommonPageRenderer {
    async fn render_template<'r>(
        &self,
        template: String,
        parent: &str,
        req: &'r Request<'_>,
    ) -> Outcome<'r> {
        let settings = req.guard::<Settings>().await.unwrap();
        let config = req.guard::<&State<AppConfig>>().await.unwrap();

        if self.is_protected && !settings.is_logged_in {
            return Outcome::from(req, custom_401(&settings));
        }

        let comments = if self.is_protected {
            Comments { data: Vec::new() }
        } else {
            req.guard::<Comments>().await.unwrap()
        };

        let context = context! {
            is_main: true,
            hide_comments: self.is_protected,
            settings: &settings,
            choices: &settings.get_choices(),
            page: &self.page,
            cat: Category::Main.get_type(),
            feeds: FeedType::get_all_main_urls(&config.card),
            meta: &config.card,
            parent,
            comments: comments.data,
        };

        Outcome::from(req, Template::render(template, context))
    }
}

#[rocket::async_trait]
impl Handler for CommonPageRenderer {
    async fn handle<'r>(&self, req: &'r Request<'_>, _: Data<'r>) -> Outcome<'r> {
        let template = if self.page.url.eq("/") {
            "home".to_string()
        } else if self.is_protected {
            format!("admin{}", String::from(&self.page.url))
        } else {
            self.page.url.replacen('/', "", 1)
        };

        CommonPageRenderer::render_template(self, template, PAGE_TEMPLATE, req).await
    }
}
