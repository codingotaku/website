/*
Copyright (C) 2021-2030 Coding Otaku (Rahul Sivananda) <contact@codingotaku.com>

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU Affero General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
details.

You should have received a copy of the GNU Affero General Public License along
with this program. If not, see <https://www.gnu.org/licenses/>.
*/

use crate::db::{fetch_all_comments, fetch_comment, fetch_comments, Db};

use regex::Regex;
use rocket::request::{FromRequest, Outcome, Request};
use rocket::serde::Serialize;
use rocket::time::format_description::well_known;
use rocket::time::OffsetDateTime;
use rocket::{form::Form, form::FromForm};
use rocket_db_pools::Connection;
use rocket_dyn_templates::handlebars::html_escape;
use uuid::Uuid;

#[derive(Debug, PartialEq, Eq)]
pub(crate) struct Comment {
    pub(crate) comment: String,
    pub(crate) mention_date: String,
    pub(crate) id: String,
    pub(crate) is_moderated: i32,
    pub(crate) name: String,
    pub(crate) post: String,
    pub(crate) mention_type: Option<String>,
    pub(crate) source: String,
    pub(crate) website: String,
}

#[derive(Serialize, Clone, Debug)]
#[serde(crate = "rocket::serde")]
pub(crate) struct CommentData {
    pub(crate) comment: String,
    pub(crate) mention_date: String,
    pub(crate) id: String,
    pub(crate) is_moderated: bool,
    pub(crate) name: String,
    pub(crate) post: String,
    pub(crate) mention_type: Option<String>,
    pub(crate) source: String,
    pub(crate) website: String,
}

#[derive(FromForm)]
pub(crate) struct CommentForm<'c> {
    pub(crate) id: Option<&'c str>,
    pub(crate) sender: &'c str,
    pub(crate) name: &'c str,
    pub(crate) website: &'c str,
    pub(crate) comment: &'c str,
    pub(crate) mention_type: Option<&'c str>,
    pub(crate) source: &'c str,
}

impl CommentData {
    fn to_html(comment: &str) -> String {
        let formatted_comment: Vec<String> = html_escape(comment)
            .split('\n')
            .map(|line| format!("<p>{}</p>", line))
            .collect();

        formatted_comment.join("\n")
    }

    pub(self) fn from_comments(comments: Vec<Comment>) -> Vec<CommentData> {
        comments
            .into_iter()
            .map(|comment| CommentData::from_comment(comment, false))
            .collect()
    }

    pub(self) fn from_comment(comment: Comment, is_raw: bool) -> CommentData {
        CommentData {
            id: comment.id,
            post: comment.post,
            name: comment.name,
            website: comment.website,
            comment: if is_raw {
                comment.comment
            } else {
                CommentData::to_html(&comment.comment)
            },
            mention_date: comment.mention_date,
            source: comment.source,
            is_moderated: comment.is_moderated.gt(&0),
            mention_type: comment.mention_type,
        }
    }
}

impl Comment {
    fn sanitize_comment(comment: &str) -> String {
        let re = Regex::new(r"(\n{2,})").unwrap();
        re.replace_all(comment.trim(), "\n").to_string()
    }

    pub(crate) fn from_form(form: &Form<CommentForm>) -> Comment {
        Comment {
            id: if form.id.is_some_and(|id| !id.is_empty()) {
                form.id.unwrap().to_string()
            } else {
                Uuid::new_v4().to_string()
            },
            post: form.sender.to_string(),
            name: if form.name.is_empty() {
                "anonymous".to_string()
            } else {
                form.name.to_string()
            },
            source: form.source.to_string(),
            website: form.website.to_string(),
            comment: Comment::sanitize_comment(form.comment),
            mention_type: form.mention_type.map(|reply_to| reply_to.to_string()),
            is_moderated: 0,
            mention_date: OffsetDateTime::now_utc()
                .format(&well_known::Iso8601::DEFAULT)
                .unwrap(),
        }
    }

    #[cfg(feature = "notification")]
    pub(crate) fn to_message(&self) -> String {
        format!(r#"{} mentioned about the post "{}""#, self.name, self.post)
    }

    pub(crate) async fn get_comments(mut db: Connection<Db>, post: &str) -> Vec<CommentData> {
        CommentData::from_comments(fetch_comments(&mut db, post).await.unwrap())
    }

    pub(crate) async fn get_all_comments(mut db: Connection<Db>) -> Vec<CommentData> {
        CommentData::from_comments(fetch_all_comments(&mut db).await.unwrap())
    }

    pub(crate) async fn get_comment(mut db: Connection<Db>, id: &str) -> CommentData {
        let comment = fetch_comment(&mut db, id).await.unwrap();
        CommentData::from_comment(comment, true)
    }
}

pub(crate) struct Comments {
    pub(crate) data: Vec<CommentData>,
}

#[rocket::async_trait]
impl<'r> FromRequest<'r> for Comments {
    type Error = std::convert::Infallible;

    async fn from_request(request: &'r Request<'_>) -> Outcome<Self, Self::Error> {
        let url = request.uri().path().to_string();
        let db = request.guard::<Connection<Db>>().await;

        let comments = Comment::get_comments(db.unwrap(), &url).await;
        Outcome::Success(Comments { data: comments })
    }
}
