/*
Copyright (C) 2021-2030 Coding Otaku (Rahul Sivananda) <contact@codingotaku.com>

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU Affero General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
details.

You should have received a copy of the GNU Affero General Public License along
with this program. If not, see <https://www.gnu.org/licenses/>.
*/

use core::fmt;
use regex::CaptureMatches;
use rocket::futures::future;
use rocket::serde::{json::from_str, Deserialize, Serialize};
use rocket::tokio::fs::read_to_string;

use crate::metadata::Category;

#[derive(Serialize, Clone)]
#[serde(crate = "rocket::serde")]
pub(crate) struct Toc<'t> {
    pub level: i32,
    pub id: &'t str,
    pub text: &'t str,
    pub children: Vec<Toc<'t>>,
}

#[derive(Serialize)]
#[serde(crate = "rocket::serde")]
pub(crate) struct PageNav<'p> {
    pub previous: Option<&'p Page>,
    pub next: Option<&'p Page>,
    pub has_any: bool,
}

#[derive(Clone, Default)]
pub(crate) struct Pages {
    pub items: Vec<Page>,
    pub tags: Vec<String>,
}

#[derive(Clone, Serialize, Deserialize)]
#[serde(crate = "rocket::serde")]
pub(crate) struct Author {
    author: String,
    link: String,
}

#[derive(Clone, Serialize, Deserialize)]
#[serde(crate = "rocket::serde")]
pub(crate) struct Banner {
    pub file: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub credit: Option<Author>,
}

#[derive(Clone, Serialize, Deserialize)]
#[serde(crate = "rocket::serde")]
pub(crate) struct Page {
    pub url: String,
    pub title: String,
    pub desc: String,
    pub date: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub banner: Option<Banner>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub ext_url: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub last_updated: Option<String>,
    pub tags: Vec<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub status: Option<String>,
}

impl fmt::Display for Toc<'_> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        if self.children.is_empty() {
            write!(f, "<li><a href=#{}>{}</a></li>", self.id, self.text)
        } else {
            write!(
                f,
                "<li><a href=#{}>{}</a><ol>{}</ol></li>",
                self.id,
                self.text,
                Toc::display(&self.children)
            )
        }
    }
}

impl<'t> Toc<'t> {
    fn from_match(result: regex::Captures<'_>, level: i32) -> Option<Toc> {
        let id = result.get(2);
        let text = result.get(3);

        if let (Some(id), Some(text)) = (id, text) {
            Some(Toc {
                level,
                id: id.as_str(),
                text: text.as_str(),
                children: Vec::new(),
            })
        } else {
            None
        }
    }

    fn add_child(&mut self, child: Toc<'t>) {
        self.children.push(child);
    }

    fn last_child(&mut self) -> Option<&mut Toc<'t>> {
        self.children.last_mut()
    }

    pub fn from(results: CaptureMatches<'t, 't>) -> Vec<Toc<'t>> {
        let mut tocs = Vec::<Toc>::new();
        results.for_each(|result| {
            let Some(level_match) = result.get(1) else {
                return;
            };

            let level: i32 = level_match.as_str().parse().unwrap();

            let Some(toc) = Toc::from_match(result, level) else {
                return;
            };

            if toc.level == 2 {
                tocs.push(toc);
                return;
            }

            let Some(last_toc) = tocs.last_mut() else {
                return;
            };

            if toc.level == 3 {
                last_toc.add_child(toc);
                return;
            }

            let Some(last_toc) = last_toc.last_child() else {
                return;
            };

            if toc.level == 4 {
                last_toc.add_child(toc);
                return;
            }

            let Some(last_toc) = last_toc.last_child() else {
                return;
            };

            if toc.level == 5 {
                last_toc.add_child(toc);
                return;
            }

            if let Some(last_toc) = last_toc.last_child() {
                last_toc.add_child(toc);
            }
        });

        tocs
    }

    pub fn display(tocs: &[Toc]) -> String {
        tocs.iter().map(ToString::to_string).collect()
    }
}

impl Page {
    pub fn has_tag(&self, tag_search: &str) -> bool {
        self.tags
            .iter()
            .any(|tag| tag.eq_ignore_ascii_case(tag_search))
    }

    pub fn url_eq(&self, path: &str) -> bool {
        self.url.eq(path)
    }

    pub fn last_modified(&self) -> &String {
        self.last_updated.as_ref().unwrap_or(&self.date)
    }
}

fn filter_pages_for_category(category: &Category, all_pages: Vec<Page>) -> Vec<Page> {
    match category {
        Category::MainsForTag(tag)
        | Category::BlogsForTag(tag)
        | Category::ProjectsForTag(tag)
        | Category::StoriesForTag(tag)
        | Category::ChaptersForTag(_, tag) => all_pages
            .into_iter()
            .filter(|item| item.has_tag(tag))
            .collect(),
        _ => all_pages,
    }
}

fn sort_by_last_modified(pages: &mut [Page]) {
    pages.sort_by_cached_key(|current| std::cmp::Reverse(current.last_modified().to_owned()));
}

async fn find_from_resource<'p>(category: &'p Category<'p>) -> Result<Pages, std::io::Error> {
    let file_content = read_to_string(format!("resources/{}.json", category.get_type())).await?;

    let pages: Vec<Page> = from_str(&file_content)?;

    let mut all_tags: Vec<String> = pages.iter().flat_map(|page| page.tags.clone()).collect();

    all_tags.sort();
    all_tags.dedup();

    let mut filtered_pages = filter_pages_for_category(category, pages);

    match category {
        Category::ChaptersForStory(_) => {}
        _ => sort_by_last_modified(&mut filtered_pages),
    };

    Ok(Pages {
        tags: all_tags,
        items: filtered_pages,
    })
}

impl Pages {
    pub async fn new(category: Category<'_>) -> Pages {
        match category {
            Category::All => Pages::all().await,
            Category::AllForTag(tag) => Pages::all_for_tag(tag).await,
            _ => find_from_resource(&category).await.unwrap_or_default(),
        }
    }

    pub fn exclude_by_url(&self, filter_name: &str) -> Vec<&Page> {
        self.items
            .iter()
            .filter(|item| !item.url_eq(filter_name))
            .collect()
    }

    pub fn find_by_url(&self, path: &str) -> Option<&Page> {
        self.items.iter().find(|item| item.url_eq(path))
    }

    pub fn get_page_nav(&self, page_name: &str) -> PageNav {
        let position = self.items.iter().position(|page| page.url_eq(page_name));

        let index = position.unwrap_or(0);

        PageNav {
            previous: if index < self.items.len() {
                self.items.get(index + 1)
            } else {
                None
            },
            next: if index > 0 {
                self.items.get(index - 1)
            } else {
                None
            },
            has_any: self.items.len() > 1,
        }
    }

    pub fn recent_excluding_url(&self, filter_url: &str) -> Vec<&Page> {
        let pages = self.exclude_by_url(filter_url);
        pages.chunks(3).next().unwrap_or_default().to_vec()
    }

    async fn from_categories(categories: Vec<Category<'_>>) -> Pages {
        let pages_vec = future::join_all(
            categories
                .iter()
                .map(|category| find_from_resource(category)),
        )
        .await;

        let mut items = Vec::<Page>::new();
        let mut tags = Vec::<String>::new();
        pages_vec.into_iter().flatten().for_each(|pages| {
            items.extend(pages.items);
            tags.extend(pages.tags);
        });

        tags.sort();
        tags.dedup();

        Pages { items, tags }
    }

    pub async fn all() -> Pages {
        let mut categories = Vec::<Category>::from([
            Category::Main,
            Category::Blogs,
            Category::Projects,
            Category::Stories,
        ]);

        let stories = find_from_resource(&Category::Stories)
            .await
            .unwrap_or_default()
            .items;

        categories.extend(
            stories
                .iter()
                .map(|page| Category::ChaptersForStory(page.url.split('/').last().unwrap())),
        );

        Pages::from_categories(categories).await
    }

    pub async fn all_for_tag(tag: &str) -> Pages {
        let mut categories = vec![
            Category::MainsForTag(tag),
            Category::BlogsForTag(tag),
            Category::ProjectsForTag(tag),
            Category::StoriesForTag(tag),
        ];

        let stories = find_from_resource(&Category::Stories)
            .await
            .unwrap_or_default()
            .items;

        categories.extend(
            stories
                .iter()
                .map(|page| Category::ChaptersForTag(page.url.split('/').last().unwrap(), tag)),
        );

        Pages::from_categories(categories).await
    }
}
