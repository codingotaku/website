/*
Copyright (C) 2021-2030 Coding Otaku (Rahul Sivananda) <contact@codingotaku.com>

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU Affero General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
details.

You should have received a copy of the GNU Affero General Public License along
with this program. If not, see <https://www.gnu.org/licenses/>.
*/

use rocket::serde::Serialize;

#[derive(Serialize)]
#[serde(crate = "rocket::serde")]
pub(crate) struct Feed<'f> {
    pub version: &'f str,
    pub title: String,
    pub home_page_url: &'f str,
    pub feed_url: String,
    pub icon: String,
    pub language: &'f str,
    pub items: Vec<FeedItem>,
    pub authors: Vec<Authors<'f>>,
}

#[derive(Serialize)]
#[serde(crate = "rocket::serde")]
pub(crate) struct Authors<'f> {
    pub name: &'f str,
    pub url: &'f str,
    pub avatar: String,
}

#[derive(Serialize)]
#[serde(crate = "rocket::serde")]
pub(crate) struct FeedItem {
    pub id: String,
    pub title: String,
    pub summary: String,
    pub content_html: String,
    pub url: String,
    pub date_published: String,
    pub banner_image: String,
    pub tags: Vec<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub external_url: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub date_modified: Option<String>,
}
