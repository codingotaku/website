/*
Copyright (C) 2021-2030 Coding Otaku (Rahul Sivananda) <contact@codingotaku.com>

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU Affero General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
details.

You should have received a copy of the GNU Affero General Public License along
with this program. If not, see <https://www.gnu.org/licenses/>.
*/

use rocket::form::FromForm;
use rocket::serde::Serialize;

#[derive(FromForm)]
pub(crate) struct IndieAuthForm {
    pub me: String,
    pub client_id: String,
    pub redirect_uri: String,
    pub state: String,
    pub response_type: Option<String>,
}

#[derive(FromForm)]
pub(crate) struct IndieAuthCodeVerification {
    pub code: String,
    pub client_id: String,
    pub redirect_uri: String,
}

#[derive(Serialize, Clone)]
#[serde(crate = "rocket::serde")]
pub(crate) struct IndieAuthResponse {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub me: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub error: Option<String>,
}

#[derive(Serialize, Clone)]
#[serde(crate = "rocket::serde")]
pub(crate) struct IndieAuthCode {
    pub id: String,
    pub client_id: String,
    pub redirect_uri: String,
    pub code: String,
    pub created_on: String,
    pub expires_on: String,
}
