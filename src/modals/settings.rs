/*
Copyright (C) 2021-2030 Coding Otaku (Rahul Sivananda) <contact@codingotaku.com>

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU Affero General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
details.

You should have received a copy of the GNU Affero General Public License along
with this program. If not, see <https://www.gnu.org/licenses/>.
*/

use crate::utils::db::{does_user_exist, Db};
use rocket::request::{FromRequest, Outcome, Request};
use rocket::serde::Serialize;
use rocket::{form::Form, form::FromForm, http::CookieJar};
use rocket_db_pools::Connection;

enum SettingType<'s> {
    Theme(&'s str),
    Width(&'s str),
    Size(&'s str),
    Opener(&'s str),
}

#[derive(Serialize, Clone)]
#[serde(crate = "rocket::serde")]
pub(crate) struct Setting<'s> {
    pub metadata: Option<&'s str>,
    pub name: &'s str,
    pub value: &'s str,
}

#[derive(Serialize)]
#[serde(crate = "rocket::serde")]
pub(crate) struct Settings<'s> {
    pub clear: bool,
    pub commenter: Option<String>,
    pub is_logged_in: bool,
    pub is_open: bool,
    pub opener: &'s Setting<'s>,
    pub sender: Option<String>,
    pub size: &'s Setting<'s>,
    pub theme: &'s Setting<'s>,
    pub width: &'s Setting<'s>,
}

#[derive(Serialize)]
#[serde(crate = "rocket::serde")]
pub(crate) struct Choice<'s> {
    pub is_selected: bool,
    pub metadata: Option<&'s str>,
    pub value: Setting<'s>,
}

#[derive(Serialize)]
#[serde(crate = "rocket::serde")]
pub(crate) struct Choices<'s> {
    pub link_openers: Vec<Choice<'s>>,
    pub sizes: Vec<Choice<'s>>,
    pub themes: Vec<Choice<'s>>,
    pub widths: Vec<Choice<'s>>,
}

#[derive(FromForm)]
pub(crate) struct SettingsForm<'s> {
    pub clear: bool,
    pub opener: &'s str,
    pub sender: &'s str,
    pub size: &'s str,
    pub theme: &'s str,
    pub width: &'s str,
}

#[rocket::async_trait]
impl<'r> FromRequest<'r> for Settings<'r> {
    type Error = std::convert::Infallible;

    async fn from_request(request: &'r Request<'_>) -> Outcome<Self, Self::Error> {
        let db = request.guard::<Connection<Db>>().await.unwrap();
        let settings = Settings::from_cookie_jar(request.cookies(), db).await;
        Outcome::Success(settings)
    }
}

impl<'s> Settings<'s> {
    pub(crate) const LINK_OPENERS: [Setting<'s>; 2] = [
        Setting {
            value: "self",
            name: "Open in the same tab (default)",
            metadata: None,
        },
        Setting {
            value: "new",
            name: "Open in a new tab",
            metadata: Some(r#"target="_blank""#),
        },
    ];

    pub(crate) const THEMES: [Setting<'s>; 5] = [
        Setting {
            value: "system",
            name: "System theme (default)",
            metadata: Option::None,
        },
        Setting {
            value: "dark",
            name: "Dark theme",
            metadata: Some("#282a36"),
        },
        Setting {
            value: "light",
            name: "Light theme",
            metadata: Some("#fff"),
        },
        Setting {
            value: "pink",
            name: "Pink theme",
            metadata: Some("#ff58af"),
        },
        Setting {
            value: "eink",
            name: "E-Ink theme",
            metadata: Some("#989898"),
        },
    ];

    pub(crate) const WIDTHS: [Setting<'s>; 4] = [
        Setting {
            value: "a5",
            name: "A5 paper width",
            metadata: Option::None,
        },
        Setting {
            value: "a4",
            name: "A4 paper width (default)",
            metadata: Option::None,
        },
        Setting {
            value: "a3",
            name: "A3 paper width",
            metadata: Option::None,
        },
        Setting {
            value: "full",
            name: "Fit window",
            metadata: Option::None,
        },
    ];

    pub(crate) const SIZES: [Setting<'s>; 7] = [
        Setting {
            value: "normal",
            name: "Normal (default)",
            metadata: Option::None,
        },
        Setting {
            value: "large",
            name: "Big",
            metadata: Option::None,
        },
        Setting {
            value: "x-large",
            name: "Bigger",
            metadata: Option::None,
        },
        Setting {
            value: "xx-large",
            name: "Huge",
            metadata: Option::None,
        },
        Setting {
            value: "small",
            name: "Small",
            metadata: Option::None,
        },
        Setting {
            value: "x-small",
            name: "Smaller",
            metadata: Option::None,
        },
        Setting {
            value: "xx-small",
            name: "Tiny",
            metadata: Option::None,
        },
    ];

    fn get_setting(setting_type: SettingType) -> &'s Setting<'s> {
        match setting_type {
            SettingType::Opener(value) => Settings::LINK_OPENERS
                .iter()
                .find(|theme| theme.value == value)
                .unwrap_or(&Settings::LINK_OPENERS[0]),
            SettingType::Size(value) => Settings::SIZES
                .iter()
                .find(|theme| theme.value == value)
                .unwrap_or(&Settings::SIZES[0]),
            SettingType::Theme(value) => Settings::THEMES
                .iter()
                .find(|theme| theme.value == value)
                .unwrap_or(&Settings::THEMES[0]),
            SettingType::Width(value) => Settings::WIDTHS
                .iter()
                .find(|theme| theme.value == value)
                .unwrap_or(&Settings::WIDTHS[0]),
        }
    }

    fn get_choice(&self, settings: Vec<Setting<'s>>, setting: &Setting<'s>) -> Vec<Choice> {
        settings
            .into_iter()
            .map(|item| Choice {
                is_selected: item.value == setting.value,
                metadata: item.metadata,
                value: item,
            })
            .collect()
    }

    pub(crate) fn get_choices(&self) -> Choices {
        Choices {
            link_openers: self.get_choice(Settings::LINK_OPENERS.to_vec(), self.opener),
            sizes: self.get_choice(Settings::SIZES.to_vec(), self.size),
            themes: self.get_choice(Settings::THEMES.to_vec(), self.theme),
            widths: self.get_choice(Settings::WIDTHS.to_vec(), self.width),
        }
    }

    fn read_cookie(cookies: &'s CookieJar<'s>, key: &str) -> Option<String> {
        cookies.get_private(key).map(|c| c.value().to_string())
    }

    pub(crate) fn from_form(form: Form<SettingsForm>) -> Settings<'s> {
        let theme = Settings::get_setting(SettingType::Theme(form.theme));
        let width = Settings::get_setting(SettingType::Width(form.width));
        let opener = Settings::get_setting(SettingType::Opener(form.opener));
        let size = Settings::get_setting(SettingType::Size(form.size));
        Settings {
            clear: form.clear,
            commenter: Option::None,
            is_logged_in: false,
            opener,
            sender: Some(form.sender.to_string()),
            size,
            theme,
            width,
            is_open: false,
        }
    }

    pub(crate) async fn from_cookie_jar(
        cookies: &CookieJar<'_>,
        mut db: Connection<Db>,
    ) -> Settings<'s> {
        let theme = Settings::read_cookie(cookies, "theme").unwrap_or("system".to_string());
        let width = Settings::read_cookie(cookies, "width").unwrap_or("a4".to_string());
        let opener = Settings::read_cookie(cookies, "opener").unwrap_or("self".to_string());
        let size = Settings::read_cookie(cookies, "size").unwrap_or("normal".to_string());

        let is_open = Settings::read_cookie(cookies, "settings_status").is_some();
        if let Some(setting_cookie) = cookies.get_private("settings_status") {
            cookies.remove_private(setting_cookie);
        }

        let is_logged_in = if let Some(user) = cookies.get_private("user") {
            does_user_exist(&mut db, user.value()).await
        } else {
            false
        };

        let commenter = cookies
            .get_private("commenter")
            .map(|commenter| commenter.value().to_string());

        Settings {
            clear: false,
            commenter,
            is_logged_in,
            opener: Settings::get_setting(SettingType::Opener(&opener)),
            sender: Option::None,
            size: Settings::get_setting(SettingType::Size(&size)),
            theme: Settings::get_setting(SettingType::Theme(&theme)),
            width: Settings::get_setting(SettingType::Width(&width)),
            is_open,
        }
    }
}
