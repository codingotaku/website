/*
Copyright (C) 2021-2030 Coding Otaku (Rahul Sivananda) <contact@codingotaku.com>

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU Affero General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
details.

You should have received a copy of the GNU Affero General Public License along
with this program. If not, see <https://www.gnu.org/licenses/>.
*/

use crate::metadata::Category;
use crate::modals::page::{Page, Pages};
use regex::Regex;
use rocket::futures::future;
use rocket::serde::{Deserialize, Serialize};
use rocket::{Orbit, Rocket};
use rocket_dyn_templates::{context, Template};

fn generate_search_page(
    page: &Page,
    is_main: bool,
    has_content: bool,
    rocket: &Rocket<Orbit>,
) -> SearchPage {
    let url = if page.url.eq("/") { "home" } else { &page.url };

    let content = if has_content {
        let text = Template::show(
            rocket,
            url.replacen('/', "", 1),
            context! { page, parent:"partials/blank", is_main },
        )
        .unwrap();
        strip(&text)
    } else {
        String::new()
    };

    let title = page
        .status
        .as_ref()
        .map_or(page.title.to_string(), |status| {
            format!("{} - {}", page.title, status)
        });

    SearchPage {
        title,
        url: page.url.clone(),
        tags: page.tags.to_owned(),
        summary: page.desc.to_owned(),
        content,
    }
}

#[derive(Debug, Clone, Deserialize, Serialize, PartialEq, Eq)]
#[serde(crate = "rocket::serde")]
pub struct SearchPage {
    pub url: String,
    pub title: String,
    pub tags: Vec<String>,
    pub summary: String,
    pub content: String,
}

pub(crate) fn sanitize_search(text: &str) -> String {
    let result = strip(text);
    let stripper = Regex::new(r"[^\w\d\^.+ ]").unwrap();
    stripper.replace_all(&result, " ").trim().to_string()
}

pub(crate) fn strip(text: &str) -> String {
    let stripper = Regex::new(r"<[^>]*>").unwrap();
    stripper.replace_all(text, " ").to_string()
}

pub(crate) fn split_tags(tags: String) -> Vec<String> {
    tags.split(',')
        .filter_map(|tag| {
            if tag.is_empty() {
                None
            } else {
                Some(tag.to_string())
            }
        })
        .collect()
}

pub(crate) fn get_match_context(text: String) -> String {
    let stripper = Regex::new(r"(.*<mark>.+</mark>.*)").unwrap();

    let mut results: Vec<String> = stripper
        .captures_iter(&text)
        .take(3)
        .map(|capture| capture.extract())
        .map(|(_, [line])| line.to_string())
        .collect();

    if results.is_empty() {
        results.push(if text.len() > 150 {
            text[..150].to_string()
        } else {
            text
        });
    }

    results.join(" [...] ")
}

pub(crate) async fn get_search_templates(rocket: &Rocket<Orbit>) -> Vec<SearchPage> {
    let mut pages = Vec::<SearchPage>::new();

    let (mains, blogs, projects, stories) = future::join4(
        Pages::new(Category::Main),
        Pages::new(Category::Blogs),
        Pages::new(Category::Projects),
        Pages::new(Category::Stories),
    )
    .await;

    for page in mains.items {
        pages.push(generate_search_page(&page, true, true, rocket));
    }

    let chapter_cats = stories.items.iter().map(|story| {
        pages.push(generate_search_page(story, false, false, rocket));
        let url = story.url.split('/').last().unwrap();
        Category::ChaptersForStory(url)
    });

    let all_chapters = future::join_all(
        chapter_cats
            .into_iter()
            .map(|chapter_cat| Pages::new(chapter_cat)),
    )
    .await
    .into_iter()
    .flat_map(|chapters| chapters.items);

    let search_pages = blogs
        .items
        .into_iter()
        .chain(projects.items.into_iter())
        .chain(all_chapters)
        .map(|page| generate_search_page(&page, false, true, rocket));

    pages.extend(search_pages);
    pages
}
