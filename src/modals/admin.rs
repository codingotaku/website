/*
Copyright (C) 2021-2030 Coding Otaku (Rahul Sivananda) <contact@codingotaku.com>

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU Affero General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
details.

You should have received a copy of the GNU Affero General Public License along
with this program. If not, see <https://www.gnu.org/licenses/>.
*/

use rocket::serde::Serialize;
use rocket::{form::Form, form::FromForm};

use crate::modals::search::sanitize_search;

#[derive(Serialize, Clone)]
#[serde(crate = "rocket::serde")]
pub(crate) struct User {
    pub username: String,
    pub password: String,
}

#[derive(FromForm)]
pub(crate) struct LoginForm {
    pub username: String,
    pub password: String,
    pub redirect_path: Option<String>,
}

#[derive(FromForm)]
pub(crate) struct UpdateForm {
    pub password: String,
    pub updated: String,
    pub retype: String,
}

impl User {
    pub(crate) fn from_form(form: &Form<LoginForm>) -> User {
        User {
            username: sanitize_search(&form.username).to_string(),
            password: form.password.to_string(),
        }
    }
}
