FROM docker.io/library/rust:1.84-slim-bullseye AS builder

RUN apt update && apt install -y libssl-dev pkg-config

ARG pkg=website

WORKDIR /website

COPY . .

RUN --mount=type=cache,target=/website/target \
    --mount=type=cache,target=/usr/local/cargo/registry \
    --mount=type=cache,target=/usr/local/cargo/git \
    set -eux; \
    cargo build --release; \
    objcopy --compress-debug-sections target/release/$pkg ./main
