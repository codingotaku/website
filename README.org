* Website

#+CAPTION: Please don't upload to GitHub.
[[https://nogithub.codeberg.page][https://nogithub.codeberg.page/badge.svg]]

This is my personal website containing my blogs, projects, things I do, and ways to contact me.
This website is written in Rust and Handlebars, partially supporting [[https://indieweb.org/][indieweb]] while keeping accessibility and minimalism in mind.
You are free to clone this repository and use it as you wish, as long as you abide by the [[#sec:license][license]].

** Validators

| Type               | Validaton Check                                        |
|--------------------|--------------------------------------------------------|
| CSS                |[[https://jigsaw.w3.org/css-validator/validator?uri=https%3A%2F%2Fcodingotaku.com%2F&profile=css3svg&usermedium=all&warning=1&vextwarning=&lang=en][https://jigsaw.w3.org/css-validator/images/vcss-blue.png]]|
| Blog Atom Feed     |[[https://validator.w3.org/feed/check.cgi?url=https%3A//codingotaku.com/blogs/atom.xml][https://validator.w3.org/feed/images/valid-atom.png]]     |
| Projects Atom Feed |[[https://validator.w3.org/feed/check.cgi?url=https%3A//codingotaku.com/projects/atom.xml][https://validator.w3.org/feed/images/valid-atom.png]]     |
| Stories Atom Feed  |[[https://validator.w3.org/feed/check.cgi?url=https%3A//codingotaku.com/stories/atom.xml][https://validator.w3.org/feed/images/valid-atom.png]]     |


** What it supports

This website partially supports IndeiWeb with [[https://indieweb.org/IndieAuth][indie-auth]], [[https://microformats.org/wiki/h-entry][h-entry]], and [[https://microformats.org/wiki/h-card][h-card]] supports built in. So it should be fairly easy to crawl my website using [[https://microformats.org/wiki/tools][microformat tools]]!

To see everything this project support related to IndieWeb, have a look at [[https://codingotaku.com/am-i-indieweb-yet][am-i-indieweb-yet]] page.
I have a long-term goal of being completely indie-web supported with other features like webmention, but I'm a bit pre-occupied to do it at the moment.

** FAQ

*** Can I use your blog?

Yes! But there are lots of hard-coded texts and references to my name and work in the repo, cleaning it up is a work in progress, so if you want to use my blog, please replace them with your details or remove them.

*** How do I build?

You need [[https://www.rust-lang.org/][rust]] installed in the system, once you have it, do the following.

1. Clone this repository
2. Run =setup.sh= - this will just add a pre-commit hook to update last modified date of the posts.
4. Copy the =App.toml.example= file to a new file called =App.toml=
    1. Update =secret_key= with a secret key of your own, read the [[https://rocket.rs/v0.5-rc/guide/configuration/#secret-key][rocket configuration guide]] to know what it does and how to generate one.
    2. If you have a [[https://unifiedpush.org/][unifiedpush]] server setup, update the =notify_url= with your =notification channel url= to get notifications when your post gets webmentions.
    3. Update the =[default.card]= section with your details (all are mandatory).
5. Do what I told in the answer for [What changes should you make](#what-changes-should-you-make)?
6. Once you have created new blogs and projects, add them to the JSON files in the =resources= directory (use the existing formats).
7. Test your changes, do =cargo run= to launch your new website.


To build and run the project in the production, do -

#+begin_src shell
cargo build --release # generate production binary
./target/release/website # run the binary
#+end_src

** What changes should you make

As I mentioned above, the project has too many references to my name and my work, so the following must be done.
1. Delete all files in the =templates/blogs=, and =templates/projects= folders (take a copy of one of the files if you need reference).
2. Rewrite all =*.html.hbs= files in the =templates= folder to refer your details, you can keep the one on the footer or just reference my name somewhere in your blog to let people know where you got the website source from.
3. Update =src/utils.rs= to match your details (I know, this should be in a config file instead).
4. Update the =JSON= files in the =resources= folder.
5. Remove any other references to "Coding Otaku" and "Rahul" that you can find, I might have missed something.

** LICENSE
:PROPERTIES:
:CUSTOM_ID: sec:license
:END:

Copyright (C) 2021-2030 Coding Otaku (Rahul Sivananda) <contact@codingotaku.com>

This program is free software: you can redistribute it and/or modify it under  
the terms of the GNU Affero General Public License as published by the Free  
Software Foundation, either version 3 of the License, or (at your option)  
any later version.

This program is distributed in the hope that it will be useful, but WITHOUT  
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  
FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more  
details.

You should have received a copy of the GNU Affero General Public License along  
with this program. If not, see <https://www.gnu.org/licenses/>.
